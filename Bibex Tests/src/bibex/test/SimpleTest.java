/*
 * (c) Copyright 2001-2003 Yann-Ga�l Gu�h�neuc,
 * �cole des Mines de Nantes and Object Technology International, Inc.
 * 
 * Use and copying of this software and preparation of derivative works
 * based upon this software are permitted. Any copy of this software or
 * of any derivative work must include the above copyright notice of
 * the author, this paragraph and the one after it.
 * 
 * This software is made available AS IS, and THE AUTHOR DISCLAIMS
 * ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, AND NOT WITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN,
 * ANY LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF THE AUTHOR IS ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 * 
 * All Rights Reserved.
 */
package bibex.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import junit.framework.Assert;
import junit.framework.TestCase;
import bibex.dom.BibtexFile;
import bibex.example.PrintVisitor;
import bibex.parser.BibtexParser;
import bibex.parser.ParseException;

/**
 * @author	Yann-Ga�l Gu�h�neuc
 */
public class SimpleTest extends TestCase {
	public SimpleTest(final String name) {
		super(name);
	}
	public void testPrinting() {
		final BibtexFile bibtexFile = new BibtexFile();
		final BibtexParser bibtexParser = new BibtexParser();
		try {
			bibtexParser.parse(bibtexFile, new FileReader(System
				.getProperty("user.dir")
					+ File.separatorChar + "Base/Books.bib"), true);

			final StringWriter writer1 = new StringWriter();
			final StringWriter writer2 = new StringWriter();
			bibtexFile.print(new PrintWriter(writer1));
			bibtexFile.accept(new PrintVisitor(new PrintWriter(writer2)));

			Assert.assertEquals(
				"Outputs are not equal.",
				writer1.toString(),
				writer2.toString());
		}
		catch (final FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (final ParseException e) {
			e.printStackTrace();
		}
		catch (final IOException e) {
			e.printStackTrace();
		}

	}
}
