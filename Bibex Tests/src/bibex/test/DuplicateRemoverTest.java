/* (c) Copyright 2001 and following years, Yann-Ga�l Gu�h�neuc,
 * University of Montreal.
 * 
 * Use and copying of this software and preparation of derivative works
 * based upon this software are permitted. Any copy of this software or
 * of any derivative work must include the above copyright notice of
 * the author, this paragraph and the one after it.
 * 
 * This software is made available AS IS, and THE AUTHOR DISCLAIMS
 * ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, AND NOT WITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN,
 * ANY LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF THE AUTHOR IS ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 * 
 * All Rights Reserved.
 */
package bibex.test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import junit.framework.Assert;
import junit.framework.TestCase;
import bibex.dom.BibtexFile;
import bibex.parser.BibtexParser;
import bibex.parser.ParseException;
import bibex.utils.DuplicateRemover;

public class DuplicateRemoverTest extends TestCase {
	private static final String OUTPUT_FILE =
		"tmp/DuplicateReferencesRemoved.bib";
	private final DuplicateRemover remover = new DuplicateRemover();

	public DuplicateRemoverTest(final String name) {
		super(name);
	}

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		// new File(DuplicateRemoverTest.OUTPUT_FILE).delete();
	}

	public void testDuplicateRemoverQSICCase() {
		this.remover.removeDuplicates(
			"rsc/DuplicateReferences.bib",
			DuplicateRemoverTest.OUTPUT_FILE);

		final BibtexFile bibtexFile = new BibtexFile();
		final BibtexParser bibtexParser = new BibtexParser();
		try {
			bibtexParser.parse(bibtexFile, new FileReader(
				DuplicateRemoverTest.OUTPUT_FILE), true);
		}
		catch (final FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (final ParseException e) {
			e.printStackTrace();
		}
		catch (final IOException e) {
			e.printStackTrace();
		}

		Assert.assertEquals(1, bibtexFile.getEntries().size());
	}
}
