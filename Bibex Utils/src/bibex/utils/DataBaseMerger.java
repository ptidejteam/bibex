/*
 * (c) Copyright Yann-Ga�l Gu�h�neuc,
 * University of Montr�al.
 * 
 * Use and copying of this software and preparation of derivative works
 * based upon this software are permitted. Any copy of this software or
 * of any derivative work must include the above copyright notice of
 * the author, this paragraph and the one after it.
 * 
 * This software is made available AS IS, and THE AUTHOR DISCLAIMS
 * ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, AND NOT WITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN,
 * ANY LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF THE AUTHOR IS ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 * 
 * All Rights Reserved.
 */
package bibex.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import bibex.dom.BibtexFile;
import bibex.parser.BibtexParser;
import bibex.parser.ParseException;

/**
 * @author Yann
 * @since 2008/11/01
 */
public class DataBaseMerger {
	public static void main(final String[] args) {
		if (args.length == 3) {
			final BibtexFile bibtexFile = new BibtexFile();
			final BibtexParser bibtexParser = new BibtexParser();

			final String path = args[0];

			// Yann 2008/11/01: Trick!
			// I trick the algorithm to always take my own BibTeX file first...
			final String[] realFiles = new File(path).list();
			final String[] files = new String[realFiles.length + 1];
			files[0] = "guehene";
			System.arraycopy(realFiles, 0, files, 1, realFiles.length);

			for (int i = 0; i < files.length; i++) {
				final String filePath = path + '/' + files[i];
				final File file = new File(filePath);
				if (file.isDirectory() && !files[i].equals("CVS")
						&& !files[i].equals(".svn")) {
					final String bibFilePath =
						filePath + '/' + files[i] + args[1] + ".bib";
					try {
						bibtexParser.parse(bibtexFile, new FileReader(
							bibFilePath), true);
					}
					catch (final FileNotFoundException e) {
						System.err.print("I cannot open the file \"");
						System.err.print(bibFilePath);
						System.err.println("\", is that a problem?");
						// e.printStackTrace();
					}
					catch (final ParseException e) {
						System.err.print("I cannot parse the file \"");
						System.err.print(bibFilePath);
						System.err.print("\": ");
						System.err.print(e.getMessage());
						System.err.println(", please correct the BibTeX file!");
					}
					catch (final IOException e) {
						e.printStackTrace();
					}
				}

				try {
					bibtexFile.print(new PrintWriter(new FileWriter(args[2])));
				}
				catch (final IOException e) {
					e.printStackTrace();
				}
			}
		}
		else {
			System.out
				.println("DataBaseMarger needs three arguments: the main directory where to find the sub-directories with the BibTeX data bases, the possible suffix, and the output file.");
		}
	}
}
