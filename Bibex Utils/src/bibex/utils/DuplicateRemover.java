/*
 * (c) Copyright Yann-Ga�l Gu�h�neuc,
 * University of Montr�al.
 * 
 * Use and copying of this software and preparation of derivative works
 * based upon this software are permitted. Any copy of this software or
 * of any derivative work must include the above copyright notice of
 * the author, this paragraph and the one after it.
 * 
 * This software is made available AS IS, and THE AUTHOR DISCLAIMS
 * ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, AND NOT WITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN,
 * ANY LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF THE AUTHOR IS ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 * 
 * All Rights Reserved.
 */
package bibex.utils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import bibex.dom.BibtexConcatenatedValue;
import bibex.dom.BibtexEntry;
import bibex.dom.BibtexField;
import bibex.dom.BibtexFile;
import bibex.dom.BibtexKey;
import bibex.dom.BibtexPreamble;
import bibex.dom.BibtexString;
import bibex.dom.BibtexStringDefinition;
import bibex.dom.BibtexStringReference;
import bibex.dom.BibtexToplevelComment;
import bibex.parser.BibtexParser;
import bibex.parser.ParseException;
import bibex.visitor.BibtexVisitor;

/**
 * @author Yann
 * @since  2008/11/01
 */
public class DuplicateRemover {
	public class MapBuilder implements BibtexVisitor {
		private BibtexEntry enclosingEntry;
		private boolean begunToVisitEntries;

		public void close(final BibtexField bibtexField) {
		}
		public void close(final BibtexFile bibtexFile) {
		}
		public void close(final BibtexEntry bibtexEntry) {
		}
		public void open(final BibtexEntry bibtexEntry) {
			this.enclosingEntry = bibtexEntry;
			this.begunToVisitEntries = true;
		}
		public void open(final BibtexField bibtexField) {
		}
		public void open(final BibtexFile bibtexFile) {
		}
		public void visit(final BibtexConcatenatedValue bibtexConcatenatedValue) {
		}
		public void visit(final BibtexKey bibtexKey) {
			if (bibtexKey.getValue().equals("title")) {
				String key =
					((BibtexField) this.enclosingEntry.getFields().get(
						bibtexKey)).getValue().getValue();
				key = this.enclosingEntry.getType() + key;
				key = key.toLowerCase().trim();
				key = key.replaceAll(" ", "");
				// Remove special character that one author may use but not another
				key = key.replaceAll("\\{", "");
				key = key.replaceAll("\\}", "");

				// Naturally keep only one (the first one, 
				// because it should be mine, see DataBaseMerger).
				if (!DuplicateRemover.this.titlesEntries.containsKey(key)) {
					DuplicateRemover.this.titlesEntries.put(
						key,
						this.enclosingEntry);
				}
			}
		}
		public void visit(final BibtexPreamble bibtexPreamble) {
			if (!this.begunToVisitEntries) {
				DuplicateRemover.this.topLevelComments.append(bibtexPreamble);
			}
		}
		public void visit(final BibtexString bibtexString) {
		}
		public void visit(final BibtexStringDefinition bibtexStringDefinition) {
			if (!this.begunToVisitEntries) {
				DuplicateRemover.this.topLevelComments
					.append(bibtexStringDefinition);
			}
		}
		public void visit(final BibtexStringReference bibtexStringReference) {
		}
		public void visit(final BibtexToplevelComment bibtexToplevelComment) {
			if (!this.begunToVisitEntries) {
				DuplicateRemover.this.topLevelComments
					.append(bibtexToplevelComment);
			}
		}
	}
	public static void main(final String[] args) {
		if (args.length == 2) {
			final DuplicateRemover remover = new DuplicateRemover();
			remover.removeDuplicates(args[0], args[1]);
		}
		else {
			System.out
				.println("DuplicateRemover needs the paths of an input and an output BibTeX database.");
		}
	}

	private final StringBuffer preamble = new StringBuffer();
	private final StringBuffer stringDefinitions = new StringBuffer();
	private final StringBuffer topLevelComments = new StringBuffer();
	private final Map titlesEntries = new HashMap();

	public void removeDuplicates(
		final String anInputFile,
		final String anOuputFile) {

		final BibtexFile bibtexFile = new BibtexFile();
		final BibtexParser bibtexParser = new BibtexParser();
		try {
			bibtexParser.parse(bibtexFile, new FileReader(anInputFile), true);
			bibtexFile.accept(new DuplicateRemover.MapBuilder());

			final FileWriter writer = new FileWriter(anOuputFile);
			writer.write(this.preamble.toString());
			writer.write(this.topLevelComments.toString());
			writer.write(this.stringDefinitions.toString());
			final Iterator iterator = this.titlesEntries.values().iterator();
			while (iterator.hasNext()) {
				final BibtexEntry entry = (BibtexEntry) iterator.next();
				entry.print(new PrintWriter(writer));
			}
			writer.close();
		}
		catch (final FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (final ParseException e) {
			e.printStackTrace();
		}
		catch (final IOException e) {
			e.printStackTrace();
		}
	}
}
