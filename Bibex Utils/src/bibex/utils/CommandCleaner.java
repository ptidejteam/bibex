/*
 * (c) Copyright Yann-Ga�l Gu�h�neuc,
 * University of Montr�al.
 * 
 * Use and copying of this software and preparation of derivative works
 * based upon this software are permitted. Any copy of this software or
 * of any derivative work must include the above copyright notice of
 * the author, this paragraph and the one after it.
 * 
 * This software is made available AS IS, and THE AUTHOR DISCLAIMS
 * ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, AND NOT WITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN,
 * ANY LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF THE AUTHOR IS ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 * 
 * All Rights Reserved.
 */
package bibex.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;

/**
 * @author Yann-Ga�l Gu�h�neuc
 * @since  2007/02/22
 */
public class CommandCleaner {
	private static final String[] LATEX_COMMANDS = new String[] { "\\student{",
			"\\textsc{" };
	private static final String[] LATEX_NAMES = new String[] { "\\Caffeine",
			"\\DECOR", "\\Java", "\\PADL", "\\PMARt", "\\Ptidej", "\\Taupe",
			"\\UML" };

	public static void main(final String[] args) {
		final File inputFile = new File(args[0]);
		if (inputFile.exists()) {
			try {
				final StringBuffer output = new StringBuffer();
				final LineNumberReader reader =
					new LineNumberReader(new FileReader(inputFile));
				String readLine = "";
				try {
					while ((readLine = reader.readLine()) != null) {
						readLine =
							CommandCleaner.removeLaTeXCommandAt(readLine);
						readLine = CommandCleaner.changeNumbering(readLine);
						readLine = CommandCleaner.beautifyReferences(readLine);
						output.append(readLine);
						output.append('\n');
					}
				}
				catch (final Exception e) {
					System.err.print("Problem with line number: ");
					System.err.println(reader.getLineNumber());
					System.err.println(readLine);
					e.printStackTrace();
					System.exit(-1);
				}
				reader.close();

				final File outputFile = new File(args[1]);
				final FileWriter writer = new FileWriter(outputFile);
				writer.write(output.toString());
				writer.close();
			}
			catch (final FileNotFoundException e) {
				e.printStackTrace();
			}
			catch (final IOException e) {
				e.printStackTrace();
			}
		}
		else {
			System.err.print("I cannot find ");
			System.err.println(args[0]);
		}
	}
	private static String removeLaTeXCommandAt(final String readLine) {
		String line = readLine;
		for (int i = 0; i < CommandCleaner.LATEX_COMMANDS.length; i++) {
			String laTeXCommand = CommandCleaner.LATEX_COMMANDS[i];
			int startIndex;
			while ((startIndex = line.indexOf(laTeXCommand)) > -1) {
				int endIndex = startIndex + laTeXCommand.length();

				int numberOfBracePairs = 1;
				while (numberOfBracePairs > 0) {
					if (line.charAt(endIndex) == '{') {
						numberOfBracePairs++;
					}
					else if (line.charAt(endIndex) == '}') {
						numberOfBracePairs--;
					}
					endIndex++;
				}

				final StringBuffer buffer = new StringBuffer();
				buffer.append(line.substring(0, startIndex));
				buffer.append(line.substring(
					startIndex + laTeXCommand.length(),
					endIndex - 1));
				buffer.append(line.substring(endIndex));
				line = buffer.toString();
			}
		}
		return line;
	}
	private static String changeNumbering(final String readLine) {
		String line = readLine;
		int startIndex;
		while ((startIndex = line.indexOf('$')) > -1) {
			int endIndex = line.indexOf('^', startIndex + 1);
			final StringBuffer buffer = new StringBuffer();
			if (endIndex > -1) {
				buffer.append(line.substring(0, startIndex));
				buffer.append(line.substring(startIndex + 1, endIndex));
				buffer.append("<sup>");
				buffer.append(line.substring(
					endIndex + 1,
					endIndex = line.indexOf('$', startIndex + 1)));
				buffer.append("</sup>");
				buffer.append(line.substring(endIndex + 1));
				line = buffer.toString();
			}
			else {
				return line;
			}
		}
		return line;
	}
	private static String beautifyReferences(final String readLine) {
		String line = readLine;
		for (int i = 0; i < CommandCleaner.LATEX_NAMES.length; i++) {
			String laTeXName = CommandCleaner.LATEX_NAMES[i];
			int startIndex;
			while ((startIndex = line.indexOf(laTeXName)) > -1) {
				int endIndex = startIndex + laTeXName.length();
				final StringBuffer buffer = new StringBuffer();
				buffer.append(line.substring(0, startIndex));
				buffer.append(line.substring(startIndex + 1, endIndex));
				buffer.append(line.substring(endIndex));
				line = buffer.toString();
			}
		}
		return line;
	}
}
