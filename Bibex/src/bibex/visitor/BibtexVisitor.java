/*
 * (c) Copyright 2001-2003 Yann-Ga�l Gu�h�neuc,
 * �cole des Mines de Nantes and Object Technology International, Inc.
 * 
 * Use and copying of this software and preparation of derivative works
 * based upon this software are permitted. Any copy of this software or
 * of any derivative work must include the above copyright notice of
 * the author, this paragraph and the one after it.
 * 
 * This software is made available AS IS, and THE AUTHOR DISCLAIMS
 * ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, AND NOT WITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN,
 * ANY LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF THE AUTHOR IS ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 * 
 * All Rights Reserved.
 */
package bibex.visitor;

import bibex.dom.BibtexConcatenatedValue;
import bibex.dom.BibtexEntry;
import bibex.dom.BibtexField;
import bibex.dom.BibtexFile;
import bibex.dom.BibtexKey;
import bibex.dom.BibtexPreamble;
import bibex.dom.BibtexString;
import bibex.dom.BibtexStringDefinition;
import bibex.dom.BibtexStringReference;
import bibex.dom.BibtexToplevelComment;

/**
 * @author 	Yann-Ga�l Gu�h�neuc
 */
public interface BibtexVisitor {
	void close(final BibtexEntry bibtexEntry);
	void close(final BibtexField bibtexField);
	void close(final BibtexFile bibtexFile);
	void open(final BibtexEntry bibtexEntry);
	void open(final BibtexField bibtexField);
	void open(final BibtexFile bibtexFile);
	void visit(final BibtexConcatenatedValue bibtexConcatenatedValue);
	void visit(final BibtexKey bibtexKey);
	void visit(final BibtexPreamble bibtexPreamble);
	void visit(final BibtexString bibtexString);
	void visit(final BibtexStringDefinition bibtexStringDefinition);
	void visit(final BibtexStringReference bibtexStringReference);
	void visit(final BibtexToplevelComment bibtexToplevelComment);
}
