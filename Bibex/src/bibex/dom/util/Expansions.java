/*
 * Created on Mar 20, 2003
 *
 * @author henkel@cs.colorado.edu
 * 
 */
package bibex.dom.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import bibex.dom.BibtexAbstractEntry;
import bibex.dom.BibtexAbstractValue;
import bibex.dom.BibtexConcatenatedValue;
import bibex.dom.BibtexConstants;
import bibex.dom.BibtexEntry;
import bibex.dom.BibtexFile;
import bibex.dom.BibtexPreamble;
import bibex.dom.BibtexString;
import bibex.dom.BibtexStringDefinition;
import bibex.dom.BibtexStringReference;
import bibex.dom.BibtexToplevelComment;

/**
 * @author henkel
 */
public final class Expansions {

	public static class ExpansionException extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = -1867981824978848153L;

		ExpansionException(final String message) {
			super(message);
		}
	}

	private static final HashSet monthAbbreviations = new HashSet();

	static {
		for (int i = 0; i < BibtexConstants.MONTH_ABBREVIATIONS.length; i++) {
			Expansions.monthAbbreviations
				.add(BibtexConstants.MONTH_ABBREVIATIONS[i]);
		}
	}

	/**
	 * Note: If you don't call expandStringReferences first, this function may lead to
	 * inconsistent string references.
	 * 
	 * @param file
	 */
	public static void expandCrossReferences(final BibtexFile bibtexFile)
			throws ExpansionException {
		final HashMap entryKey2Entry = new HashMap();
		final ArrayList entriesWithCrossReference = new ArrayList();
		for (final Iterator entryIt = bibtexFile.getEntries().iterator(); entryIt
			.hasNext();) {
			final BibtexAbstractEntry abstractEntry =
				(BibtexAbstractEntry) entryIt.next();
			if (!(abstractEntry instanceof BibtexEntry)) {
				continue;
			}
			final BibtexEntry entry = (BibtexEntry) abstractEntry;
			entryKey2Entry.put(entry.getKey().toLowerCase(), abstractEntry);
			if (entry.getFields().containsKey("crossref")) {
				entriesWithCrossReference.add(entry);
			}
		}
		for (final Iterator entryIt = entriesWithCrossReference.iterator(); entryIt
			.hasNext();) {
			final BibtexEntry entry = (BibtexEntry) entryIt.next();
			final String crossrefKey =
				((BibtexString) entry.getFields().get("crossref"))
					.getValue()
					.toLowerCase();
			entry.undefineField("crossref");
			final BibtexEntry crossrefEntry =
				(BibtexEntry) entryKey2Entry.get(crossrefKey);
			if (crossrefEntry == null) {
				throw new ExpansionException("Crossref key not found: \""
						+ crossrefKey + "\"");
			}
			if (crossrefEntry.getFields().containsKey("crossref")) {
				throw new ExpansionException("Nested crossref: \""
						+ crossrefKey
						+ "\" is crossreferenced but crossreferences itself \""
						+ ((BibtexString) crossrefEntry.getFields().get(
							"crossref")).getValue() + "\"");
			}
			final Map entryFields = entry.getFields();
			final Map crossrefFields = crossrefEntry.getFields();
			for (final Iterator fieldIt = crossrefFields.keySet().iterator(); fieldIt
				.hasNext();) {
				final String key = (String) fieldIt.next();
				if (!entryFields.containsKey(key)) {
					entry.addField(BibtexFile.makeField(
						key,
						(BibtexAbstractValue) crossrefFields.get(key)));
				}
			}
		}
	}

	/** 
	 * this method walks over all entries (including strings!) and expands string references.
	 * Thus, after the execution of this function, all fields contain BibtexString or
	 * BibtexNumber entries. Exception: 1) the crossref fields 2) the standard 3-letter
	 * abbreviations for months.
	 * 
	 * @param bibtexFile
	 */
	public static void expandStringReferences(
		final BibtexFile bibtexFile,
		final boolean removeStringDefinitions) throws ExpansionException {

		final HashMap stringKey2StringValue = new HashMap();
		for (final Iterator entryIt = bibtexFile.getEntries().iterator(); entryIt
			.hasNext();) {
			final BibtexAbstractEntry abstractEntry =
				(BibtexAbstractEntry) entryIt.next();
			if (abstractEntry instanceof BibtexStringDefinition) {
				final BibtexStringDefinition bibtexStringDefinition =
					(BibtexStringDefinition) abstractEntry;
				final BibtexAbstractValue simplifiedValue =
					Expansions.simplify(bibtexFile, bibtexStringDefinition
						.getValue(), stringKey2StringValue);
				bibtexStringDefinition.setValue(simplifiedValue);
				if (removeStringDefinitions) {
					bibtexFile.removeEntry(bibtexStringDefinition);
				}
				;
				stringKey2StringValue.put(bibtexStringDefinition
					.getKey()
					.toLowerCase(), simplifiedValue);

			}
			else if (abstractEntry instanceof BibtexPreamble) {
				final BibtexPreamble preamble = (BibtexPreamble) abstractEntry;
				preamble.setContent(Expansions.simplify(bibtexFile, preamble
					.getContent(), stringKey2StringValue));
			}
			else if (abstractEntry instanceof BibtexEntry) {
				final BibtexEntry entry = (BibtexEntry) abstractEntry;
				for (final Iterator fieldIt =
					entry.getFields().entrySet().iterator(); fieldIt.hasNext();) {
					final Map.Entry field = (Map.Entry) fieldIt.next();
					if (!(field.getValue() instanceof BibtexString)) {
						entry.addField(BibtexFile.makeField((String) field
							.getKey(), Expansions.simplify(
							bibtexFile,
							(BibtexAbstractValue) field.getValue(),
							stringKey2StringValue)));
					}
				}
			}
			else if (abstractEntry instanceof BibtexToplevelComment) {
				// don't do anything here ...	
			}
			else {
				throw new ExpansionException(
					"Expansions.expandStringReferences(): I don't support \""
							+ abstractEntry.getClass().getName()
							+ "\". Use the force, read the source!");
			}
		}
	}

	private static BibtexAbstractValue simplify(
		final BibtexFile factory,
		final BibtexAbstractValue compositeValue,
		final HashMap stringKey2StringValue) throws ExpansionException {
		if (compositeValue instanceof BibtexString) {
			return compositeValue;
		}
		if (compositeValue instanceof BibtexStringReference) {
			final BibtexStringReference reference =
				(BibtexStringReference) compositeValue;
			final String key = reference.getValue().toLowerCase();
			if (Expansions.monthAbbreviations.contains(key)) {
				return reference;
			}
			final BibtexString simplifiedValue =
				(BibtexString) stringKey2StringValue.get(key);
			if (simplifiedValue == null) {
				throw new ExpansionException(
					"Invalid string reference (target does not exist): \""
							+ reference.getValue() + "\"");
			}
			return simplifiedValue;
		}
		if (compositeValue instanceof BibtexConcatenatedValue) {
			final BibtexConcatenatedValue concatenatedValue =
				(BibtexConcatenatedValue) compositeValue;
			final BibtexAbstractValue left =
				Expansions.simplify(
					factory,
					concatenatedValue.getLeft(),
					stringKey2StringValue);
			final BibtexAbstractValue right =
				Expansions.simplify(
					factory,
					concatenatedValue.getRight(),
					stringKey2StringValue);
			if (left instanceof BibtexString && right instanceof BibtexString) {
				return BibtexFile.makeString(((BibtexString) left).getValue()
						+ ((BibtexString) right).getValue());
			}
			else {
				return BibtexFile.makeConcatenatedValue(left, right);
			}
		}
		throw new ExpansionException(
			"Expansions.simplify(): I don't support \""
					+ compositeValue.getClass().getName()
					+ "\". Use the force, read the source!");
	}

}
