/*
 * Created on Mar 22, 2003
 *
 * @author henkel@cs.colorado.edu
 * 
 */
package bibex.dom;

/**
 * @author	henkel
 * @author	Yann-Ga�l Gu�h�neuc
 */
public interface BibtexConstants {
	String[] MONTH_ABBREVIATIONS =
		new String[] { "jan", "feb", "mar", "apr", "may", "jun", "jul", "aug",
				"sep", "oct", "nov", "dec" };

}
