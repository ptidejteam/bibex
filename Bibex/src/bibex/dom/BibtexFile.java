/*
 * Created on Mar 17, 2003
 *
 * @author henkel@cs.colorado.edu
 * 
 */
package bibex.dom;

import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import bibex.visitor.BibtexVisitor;

/**
 * This is the root of a bibtex DOM tree. It's also the factory and thus
 * the only way to create nodes.
 * 
 * @author 	henkel
 * @author 	Yann-Ga�l Gu�h�neuc
 */
public class BibtexFile extends BibtexNode implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5943378747266004872L;

	public static BibtexConcatenatedValue makeConcatenatedValue(
		final BibtexAbstractValue left,
		final BibtexAbstractValue right) {
		return new BibtexConcatenatedValue(left, right);
	}
	public static BibtexEntry makeEntry(
		final String entryType,
		final String entryKey) {
		return new BibtexEntry(entryType, entryKey);
	}
	public static BibtexField makeField(
		final String name,
		final BibtexAbstractValue value) {
		return new BibtexField(name, value);
	}
	public static BibtexPreamble makePreamble(final BibtexAbstractValue content) {
		return new BibtexPreamble(content);
	}
	public static BibtexStringReference makeReference(final String key) {
		return new BibtexStringReference(key);
	}
	public static BibtexString makeString(final String content) {
		// Content includes the quotes or curly braces around the string.
		return new BibtexString(content);
	}
	public static BibtexStringDefinition makeStringDefinition(
		final String key,
		final BibtexAbstractValue value) {
		return new BibtexStringDefinition(key, value);
	}
	public static BibtexToplevelComment makeToplevelComment(final String content) {
		return new BibtexToplevelComment(content);
	}

	private final ArrayList entries = new ArrayList();

	public void accept(final BibtexVisitor visitor) {
		visitor.open(this);

		final Iterator iterator = this.entries.iterator();
		while (iterator.hasNext()) {
			final BibtexNode node = (BibtexNode) iterator.next();
			node.accept(visitor);
		}

		visitor.close(this);
	}
	public void addEntry(final BibtexAbstractEntry entry) {
		this.entries.add(entry);
	}
	public List getEntries() {
		return Collections.unmodifiableList((List) this.entries.clone());
	}
	public void print(final PrintWriter writer) {
		for (final Iterator iter = this.entries.iterator(); iter.hasNext();) {
			final BibtexNode node = (BibtexNode) iter.next();
			node.print(writer);
		}
		writer.flush();
	}
	public void removeEntry(final BibtexAbstractEntry entry) {
		this.entries.remove(entry);
	}
	public void reset() {
		this.entries.clear();
	}
}
