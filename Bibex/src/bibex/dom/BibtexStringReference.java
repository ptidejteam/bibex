/*
 * A BibtexStringReference references a BibtexStringDefinition
 * 
 * Created on Mar 17, 2003
 *
 * @author henkel@cs.colorado.edu
 * 
 */
package bibex.dom;

import java.io.PrintWriter;
import java.io.Serializable;

/**
 * A BibtexStringReference either references a BibtexStringDefinition.
 * 
 * @author	henkel
 * @author	Yann-Ga�l Gu�h�neuc
 */
public class BibtexStringReference extends BibtexAbstractValue implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3162754053430686176L;
	private String key;

	protected BibtexStringReference(final String key) {
		this.key = key;
	}

	/**
	 * @return String
	 */
	public String getValue() {
		return this.key;
	}

	public void print(final PrintWriter writer) {
		writer.print(this.key);
	}

	/**
	 * Sets the key.
	 * @param key The key to set
	 */
	public void setKey(final String key) {
		this.key = key;
	}
}
