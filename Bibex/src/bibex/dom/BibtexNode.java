/*
 * Created on Mar 17, 2003
 *
 * @author henkel@cs.colorado.edu
 * 
 */
package bibex.dom;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import bibex.visitor.BibtexVisitor;

/**
 * @author henkel
 * @author Yann-Ga�l Gu�h�neuc
 */
public abstract class BibtexNode {
	public void accept(final BibtexVisitor visitor) {
		this.accept(visitor, "visit");
	}
	private void accept(final BibtexVisitor visitor, final String methodName) {

		final java.lang.Class[] argument =
			new java.lang.Class[] { this.getClass() };
		try {
			final Method method =
				visitor.getClass().getMethod(methodName, argument);
			method.invoke(visitor, new Object[] { this });
		}
		catch (final NoSuchMethodException nsme) {
			nsme.printStackTrace();
		}
		catch (final IllegalAccessException iae) {
			iae.printStackTrace();
		}
		catch (final InvocationTargetException ite) {
			ite.printStackTrace();
		}
	}
	abstract public void print(final PrintWriter writer);
	public String toString() {
		final StringWriter stringWriter = new StringWriter();
		final PrintWriter out = new PrintWriter(stringWriter);
		this.print(out);
		out.flush();
		return stringWriter.toString();
	}
}
