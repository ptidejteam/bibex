/*
 * Created on Mar 19, 2003
 *
 * @author henkel@cs.colorado.edu
 * 
 */
package bibex.dom;

import java.io.PrintWriter;
import java.io.Serializable;

/**
 * @author 	henkel
 * @author 	Yann-Ga�l Gu�h�neuc
 */
public class BibtexPreamble extends BibtexAbstractEntry implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6305151171260168448L;
	private BibtexAbstractValue content;

	protected BibtexPreamble(final BibtexAbstractValue content) {
		this.content = content;
	}

	/**
	 * @return BibtexAbstractValue
	 */
	public BibtexAbstractValue getContent() {
		return this.content;
	}

	/* (non-Javadoc)
	 * @see bibtex.dom.BibtexNode#printBibtex(java.io.PrintWriter)
	 */
	public void print(final PrintWriter writer) {
		writer.println("@preamble{");
		this.content.print(writer);
		writer.println("}");
	}

	/**
	 * Sets the content.
	 * @param content The content to set
	 */
	public void setContent(final BibtexAbstractValue content) {
		this.content = content;
	}

}
