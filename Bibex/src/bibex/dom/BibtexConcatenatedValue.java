/*
 * E. g. "asdf " # 19
 * 
 * Created on Mar 17, 2003
 *
 * @author henkel@cs.colorado.edu
 * 
 */
package bibex.dom;

import java.io.PrintWriter;
import java.io.Serializable;
import bibex.visitor.BibtexVisitor;

/**
 * @author	henkel
 * @author 	Yann-Ga�l Gu�h�neuc
 */
public class BibtexConcatenatedValue extends BibtexAbstractValue implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7638980487144951046L;
	private BibtexAbstractValue left, right;

	protected BibtexConcatenatedValue(
		final BibtexAbstractValue left,
		final BibtexAbstractValue right) {

		this.left = left;
		this.right = right;
	}

	public void accept(final BibtexVisitor visitor) {
		visitor.visit(this);
		this.getLeft().accept(visitor);
		this.getRight().accept(visitor);
	}

	/**
	 * @return BibtexValue
	 */
	public BibtexAbstractValue getLeft() {
		return this.left;
	}

	/**
	 * @return BibtexValue
	 */
	public BibtexAbstractValue getRight() {
		return this.right;
	}

	public String getValue() {
		return this.left.getValue() + '#' + this.right.getValue();
	}

	public void print(final PrintWriter writer) {
		this.left.print(writer);
		writer.print('#');
		this.right.print(writer);
	}

	/**
	 * Sets the left.
	 * @param left The left to set
	 */
	public void setLeft(final BibtexAbstractValue left) {
		this.left = left;
	}
	/**
	 * Sets the right.
	 * @param right The right to set
	 */
	public void setRight(final BibtexAbstractValue right) {
		this.right = right;
	}
}
