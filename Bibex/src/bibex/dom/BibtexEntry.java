/*
 * Created on Mar 17, 2003
 *
 * @author henkel@cs.colorado.edu
 * 
 */
package bibex.dom;

import java.io.PrintWriter;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import bibex.visitor.BibtexVisitor;

/**
 * @author	henkel
 * @author	Yann-Ga�l Gu�h�neuc
 */
public class BibtexEntry extends BibtexAbstractEntry implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2543132862929329251L;
	private final String entryType;
	private final String entryKey;
	private final HashMap fields = new HashMap();

	protected BibtexEntry(final String entryType, final String entryKey) {
		this.entryKey = entryKey;
		this.entryType = entryType.toLowerCase();
	}
	/**
	 * Returns a read only view of the field map.
	 * This is a mapping from java.lang.String instances (field names) to instances of
	 * BibtexAbstractValue.
	 * 
	 * @return HashMap
	 */
	public void accept(final BibtexVisitor visitor) {
		visitor.open(this);

		final Iterator iterator = this.fields.entrySet().iterator();
		while (iterator.hasNext()) {
			final Map.Entry entry = (Map.Entry) iterator.next();
			((BibtexNode) entry.getValue()).accept(visitor);
		}

		visitor.close(this);
	}
	public void addField(final BibtexField bibtexField) {
		// Yann 2003/03/28: Need a type.
		// I need to manage the fieldName as a BibtexKey
		// and BibtexAbstractValue to work with the
		// BibtexVisitor.
		this.fields.put(bibtexField.getKey(), bibtexField);
	}
	public Map getFields() {
		return Collections.unmodifiableMap((Map) this.fields.clone());
	}
	public String getKey() {
		return this.entryKey;
	}
	public String getType() {
		return this.entryType;
	}
	public void print(final PrintWriter writer) {
		writer.print('@');
		writer.print(this.entryType);
		writer.print('{');
		writer.print(this.entryKey);
		writer.println(',');

		final Iterator iterator = this.fields.entrySet().iterator();
		while (iterator.hasNext()) {
			final Map.Entry entry = (Map.Entry) iterator.next();
			writer.print('\t');
			((BibtexNode) entry.getValue()).print(writer);
			writer.println(',');
		}

		writer.println('}');
	}
	public void undefineField(final String fieldName) {
		this.fields.remove(fieldName);
	}
}
