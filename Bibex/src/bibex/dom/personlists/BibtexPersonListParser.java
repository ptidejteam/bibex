/*
 * Created on Mar 27, 2003
 *
 * @author henkel@cs.colorado.edu
 * 
 */
package bibex.dom.personlists;

import java.util.LinkedList;
import bibex.dom.BibtexString;

/**
 * @author henkel
 */
public final class BibtexPersonListParser {

	public static class Exception extends java.lang.Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = 6791780986268871104L;

		Exception(final String message) {
			super(message);
		}
	}

	private static final String COMMA = ",".intern();

	private static final String AND = "and".intern();

	public static void main(final String args[]) {
		try {
			for (int i = 0; i < args.length; i++) {
				System.out.println("-> " + args[i]);
				System.out.println("" + BibtexPersonListParser.parse(args[i]));
				System.out.println();
			}
		}
		catch (final Exception e) {
			e.printStackTrace();
		}
	}

	private static BibtexPerson makePerson(
		final String[] tokens,
		int begin,
		int end,
		final String fullEntry) throws Exception {
		if (tokens[begin].equals("others")) {
			return new BibtexPerson(null, null, null, null, true);
		}
		else {
			int numberOfCommas = 0;
			for (int i = begin; i < end; i++) {
				if (tokens[i] == BibtexPersonListParser.COMMA) {
					numberOfCommas++;
				}
			}
			if (numberOfCommas == 0) {
				final String lineage;
				if (tokens[end - 1].equals("Jr")) {
					lineage = "Jr";
					end--;
				}
				else {
					lineage = null;
				}

				int lastLowercase = -1;
				for (int i = end - 1; i > begin; i--) {
					if (Character.isLowerCase(tokens[i].charAt(0))) {
						lastLowercase = i;
						break;
					}
				}
				final String last;
				if (lastLowercase != -1) {
					// everything after that is lastname ...
					final StringBuffer buffer = new StringBuffer();
					for (int i = lastLowercase + 1; i < end; i++) {
						buffer.append(tokens[i] + " ");
					}
					last = buffer.toString().trim();
					end = lastLowercase + 1;
				}
				else {
					// the last token is the lastname ...
					last = tokens[end - 1];
					end--;
				}
				final String preLast;
				{
					int beginPreLast = end;
					for (int i = end - 1; i > begin; i--) {
						if (Character.isLowerCase(tokens[i].charAt(0))) {
							beginPreLast = i;
						}
						else {
							break;
						}
					}
					if (end - beginPreLast > 0) {
						final StringBuffer buffer = new StringBuffer();
						for (int i = beginPreLast; i < end; i++) {
							buffer.append(tokens[i] + " ");
						}
						preLast = buffer.toString().trim();
					}
					else {
						preLast = null;
					}
					end = beginPreLast;
				}
				final String first;
				if (end - begin > 0) {
					final StringBuffer buffer = new StringBuffer();
					for (int i = begin; i < end; i++) {
						buffer.append(tokens[i] + " ");
					}
					first = buffer.toString().trim();
				}
				else {
					first = null;
				}
				return new BibtexPerson(first, preLast, last, lineage, false);
			}
			else if (numberOfCommas == 1 || numberOfCommas == 2) {
				final String first, lineage, last, preLast;
				if (numberOfCommas == 1) {
					// either first or lineage is empty ...
					if (tokens[end - 1].equals("Jr")) {
						lineage = "Jr";
						first = null;
						end -= 2; // skip the comma as well.
					}
					else {
						int beginFirst = end;
						for (int i = end - 1; i >= begin; i--) {
							if (tokens[i] == BibtexPersonListParser.COMMA) {
								beginFirst = i + 1;
								break;
							}
						}
						final StringBuffer buffer = new StringBuffer();
						for (int i = beginFirst; i < end; i++) {
							buffer.append(tokens[i] + " ");
						}
						first = buffer.toString().trim();
						lineage = null;
						end = beginFirst - 1; // skip the comma as well...
					}
				}
				else { // 2 commas ...
					int beginFirst = end;
					for (int i = end - 1; i >= begin; i--) {
						if (tokens[i] == BibtexPersonListParser.COMMA) {
							beginFirst = i + 1;
							break;
						}
					}
					final StringBuffer buffer = new StringBuffer();
					for (int i = beginFirst; i < end; i++) {
						buffer.append(tokens[i] + " ");
					}
					first = buffer.toString().trim();
					end = beginFirst - 1;
					if (!tokens[end - 1].equals("Jr")) {
						throw new Exception("Expected 'Jr' instead of '"
								+ tokens[end - 1] + "'");
					}
					lineage = "Jr";
					end -= 2;
				}
				{
					// parse preLast ...
					final StringBuffer buffer = new StringBuffer();
					while (Character.isLowerCase(tokens[begin].charAt(0))) {
						buffer.append(tokens[begin++] + " ");
					}
					if (buffer.toString().trim().equals("")) {
						preLast = null;
					}
					else {
						preLast = buffer.toString().trim();
					}
				}
				{
					// parse last ...
					final StringBuffer buffer = new StringBuffer();
					while (begin < end) {
						buffer.append(tokens[begin++] + " ");
					}
					if (buffer.toString().trim().equals("")) {
						throw new Exception("Last name empty in '" + fullEntry
								+ "'");
					}
					else {
						last = buffer.toString().trim();
					}
					return new BibtexPerson(
						first,
						preLast,
						last,
						lineage,
						false);
				}
			}
			else {
				throw new Exception("Too many commas in " + fullEntry + ".");
			}
		}
	}
	public static BibtexPersonList parse(final BibtexString personList)
			throws Exception {
		return BibtexPersonListParser.parse(personList.getValue());
	}

	public static BibtexPersonList parse(final String personlist)
			throws Exception {
		final String[] tokens = BibtexPersonListParser.tokenize(personlist);
		final LinkedList persons = new LinkedList();
		int begin = 0;
		for (int i = 0; i < tokens.length; i++) {
			if (tokens[i].toLowerCase().equals(BibtexPersonListParser.AND)) {
				persons.add(BibtexPersonListParser.makePerson(
					tokens,
					begin,
					i,
					personlist));
				begin = i + 1;
			}
		}
		persons.add(BibtexPersonListParser.makePerson(
			tokens,
			begin,
			tokens.length,
			personlist));

		final BibtexPerson[] result = new BibtexPerson[persons.size()];
		persons.toArray(result);
		return new BibtexPersonList(result);
	}

	/**
	 * 
	 * 
	 * @param stringContent
	 * @return String[]
	 */
	private static String[] tokenize(String stringContent) {
		int numberOfOpenBraces = 0;
		int tokenBegin = 0;
		stringContent = stringContent + " ";
		// make sure the last character is whitespace ;-)
		final LinkedList tokens = new LinkedList(); // just some strings ...
		for (int currentPos = 0; currentPos < stringContent.length(); currentPos++) {
			switch (stringContent.charAt(currentPos)) {
				case '{' :
					numberOfOpenBraces++;
					break;
				case '}' :
					numberOfOpenBraces--;
					break;
				case ',' :
					if (numberOfOpenBraces == 0) {
						if (tokenBegin <= currentPos - 1) {
							final String potentialToken =
								stringContent
									.substring(tokenBegin, currentPos)
									.trim();
							if (!potentialToken.equals("")) {
								tokens.add(potentialToken);
							}
						}
						tokens.add(BibtexPersonListParser.COMMA);
						tokenBegin = currentPos + 1;
					}
				default :
					if (Character
						.isWhitespace(stringContent.charAt(currentPos))) {
						if (numberOfOpenBraces == 0 && tokenBegin <= currentPos) {
							final String potentialToken =
								stringContent
									.substring(tokenBegin, currentPos)
									.trim();
							if (!potentialToken.equals("")) {
								tokens.add(potentialToken);
							}
							tokenBegin = currentPos + 1;
						}
					}
			}
		}
		final String[] result = new String[tokens.size()];
		tokens.toArray(result);
		return result;
	}
}
