/*
 * Created on Mar 27, 2003
 *
 * @author henkel@cs.colorado.edu
 * 
 */
package bibex.dom.personlists;

import java.io.PrintWriter;
import bibex.dom.BibtexNode;

/**
 * @author henkel
 */
public final class BibtexPersonList extends BibtexNode {

	private final BibtexPerson[] list;

	public BibtexPersonList(final BibtexPerson[] list) {
		this.list = list;
	}

	/**
	 * Note: The returned list is live, which means that changing it changes the state of the object!
	 * 
	 * @return BibtexPerson
	 */
	public BibtexPerson[] getList() {
		return this.list;
	}

	/* (non-Javadoc)
	 * @see bibtex.dom.BibtexNode#printBibtex(java.io.PrintWriter)
	 */
	public void print(final PrintWriter writer) {
		for (int i = 0; i < this.list.length; i++) {
			if (i != 0) {
				writer.print(" and ");
			}
			this.list[i].print(writer);
		}
	}

}
