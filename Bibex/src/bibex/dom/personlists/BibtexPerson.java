/*
 * Created on Mar 27, 2003
 *
 * @author henkel@cs.colorado.edu
 * 
 */
package bibex.dom.personlists;

import java.io.PrintWriter;
import bibex.dom.BibtexNode;

/**
 * E.g. Charles Louis Xavier Joseph de la Vall{'e}e Poussin Jr:
 * <pre>
 * first = "Charles Louis Xavier Joseph"
 * preLast = "de la"
 * last = "Vall{'e}e Poussin"
 * lineage = "Jr"
 * </pre>
 * 
 * Fields that are not used are set to null. If isAndOthers is true,
 * all fields are ignored (should be null).
 * 
 * @author henkel
 */
public final class BibtexPerson extends BibtexNode {

	private String first, preLast, last, lineage;

	private boolean isOthers;
	public BibtexPerson(
		final String first,
		final String preLast,
		final String last,
		final String lineage,
		final boolean isOthers) {
		this.first = first;
		this.preLast = preLast;
		this.last = last;
		this.lineage = lineage;
		this.isOthers = isOthers;
	}

	/**
	 * @return String
	 */
	public String getFirst() {
		return this.first;
	}

	/**
	 * @return String
	 */
	public String getLast() {
		return this.last;
	}

	/**
	 * @return String
	 */
	public String getLineage() {
		return this.lineage;
	}

	/**
	 * @return String
	 */
	public String getPreLast() {
		return this.preLast;
	}

	/**
	 * @return boolean
	 */
	public boolean isOthers() {
		return this.isOthers;
	}

	/* (non-Javadoc)
	 * @see bibtex.dom.BibtexNode#printBibtex(java.io.PrintWriter)
	 */
	public void print(final PrintWriter writer) {
		if (this.isOthers) {
			writer.print("others");
		}
		else {
			if (this.preLast != null) {
				writer.print(this.preLast);
				writer.print(' ');
			}
			writer.print(this.last);
			writer.print(", ");
			if (this.lineage != null) {
				writer.print(this.lineage);
				writer.print(", ");
			}
			if (this.first != null) {
				writer.print(this.first);
			}
		}
	}

	/**
	 * Sets the first.
	 * @param first The first to set
	 */
	public void setFirst(final String first) {
		this.first = first;
	}

	/**
	 * Sets the last.
	 * @param last The last to set
	 */
	public void setLast(final String last) {
		this.last = last;
	}

	/**
	 * Sets the lineage.
	 * @param lineage The lineage to set
	 */
	public void setLineage(final String lineage) {
		this.lineage = lineage;
	}

	/**
	 * Sets the isAndOthers.
	 * @param isAndOthers The isAndOthers to set
	 */
	public void setOthers(final boolean isAndOthers) {
		this.isOthers = isAndOthers;
	}

	/**
	 * Sets the preLast.
	 * @param preLast The preLast to set
	 */
	public void setPreLast(final String preLast) {
		this.preLast = preLast;
	}

}
