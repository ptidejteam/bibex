/*
 * top-level comments are comments starting with %
 * For simplicity, we allow such comments only outside of all other entries.
 * Created on Mar 17, 2003
 *
 * @author henkel@cs.colorado.edu
 * 
 */
package bibex.dom;

import java.io.PrintWriter;
import java.io.Serializable;

/**
 * @author 	henkel
 * @author 	Yann-Ga�l Gu�h�neuc
 */
public class BibtexToplevelComment extends BibtexAbstractEntry implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8207431916958681470L;

	private String content;

	protected BibtexToplevelComment(final String content) {
		this.content = content;
	}

	/**
	 * @return String
	 */
	public String getContent() {
		return this.content;
	}

	/* (non-Javadoc)
	 * @see bibtex.dom.BibtexNode#printBibtex(java.io.PrintWriter)
	 */
	public void print(final PrintWriter writer) {
		writer.println(this.content);
	}

	/**
	 * Sets the content.
	 * @param content The content to set
	 */
	public void setContent(final String content) {
		this.content = content;
	}
}
