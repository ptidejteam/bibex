/*
 * 
 * A BibtexString is a literal string, such as "Donald E. Knuth"
 * 
 * Created on Mar 17, 2003
 *
 * @author henkel@cs.colorado.edu
 * 
 */
package bibex.dom;

import java.io.PrintWriter;
import java.io.Serializable;

/**
 * @author	henkel
 * @author 	Yann-Ga�l Gu�h�neuc
 */
public class BibtexString extends BibtexAbstractValue implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3715447342057075463L;
	//	Content does not include the quotes or curly braces around the string!
	private String content;

	/**
	 * content includes the quotes or curly braces around the string!
	 * 
	 * @param content
	 */
	protected BibtexString(final String content) {

		this.content = content;
	}

	/**
	 * content includes the quotes or curly braces around the string!
	 * 
	 * @return String
	 */
	public String getValue() {
		return this.content;
	}

	/* (non-Javadoc)
	 * @see bibtex.dom.BibtexNode#printBibtex(java.io.PrintWriter)
	 */
	public void print(final PrintWriter writer) {
		// is this really a number?
		try {
			Integer.parseInt(this.content);
			// Yann 2003/03/28: Syntax!
			// I surroung a number with curly braces,
			// to ease and to make consistent the 
			// implementation of visitors.
			writer.print('{');
			writer.print(this.content);
			writer.print('}');
		}
		catch (final NumberFormatException nfe) {
			writer.print('{');
			writer.print(this.content);
			writer.print('}');
		}
	}

	/**
	 * Sets the content.
	 * @param content The content to set
	 */
	public void setContent(final String content) {
		this.content = content;
	}

}
