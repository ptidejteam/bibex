/*
 * (c) Copyright 2001-2003 Yann-Ga�l Gu�h�neuc,
 * �cole des Mines de Nantes and Object Technology International, Inc.
 * 
 * Use and copying of this software and preparation of derivative works
 * based upon this software are permitted. Any copy of this software or
 * of any derivative work must include the above copyright notice of
 * the author, this paragraph and the one after it.
 * 
 * This software is made available AS IS, and THE AUTHOR DISCLAIMS
 * ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, AND NOT WITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN,
 * ANY LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF THE AUTHOR IS ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 * 
 * All Rights Reserved.
 */
package bibex.dom;

import java.io.PrintWriter;
import java.io.Serializable;
import bibex.visitor.BibtexVisitor;

/**
 * @author 	Yann-Ga�l Gu�h�neuc
 */
public class BibtexField extends BibtexAbstractEntry implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8681065794677430862L;
	private final BibtexKey key;
	private final BibtexAbstractValue value;

	protected BibtexField(final String name, final BibtexAbstractValue value) {

		this.key = new BibtexKey(name);
		this.value = value;
	}
	public void accept(final BibtexVisitor visitor) {
		visitor.open(this);

		((BibtexNode) this.getKey()).accept(visitor);
		((BibtexNode) this.getValue()).accept(visitor);

		visitor.close(this);
	}
	public BibtexKey getKey() {
		return this.key;
	}
	public BibtexAbstractValue getValue() {
		return this.value;
	}
	public void print(final PrintWriter writer) {
		writer.print(this.key.getValue());
		writer.print('=');
		writer.print(this.value);
	}
}
