/*
 * A string definition is something like
 * 
 * @string{ cacm = "Communications of the ACM }
 * 
 * Created on Mar 17, 2003
 *
 * @author henkel@cs.colorado.edu
 * 
 */
package bibex.dom;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import bibex.visitor.BibtexVisitor;

/**
 * @author 	henkel
 * @author 	Yann-Ga�l Gu�h�neuc
 */
public class BibtexStringDefinition extends BibtexAbstractEntry implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6234450817646828553L;

	private String key;

	private BibtexAbstractValue value;
	protected BibtexStringDefinition(
		final String key,
		final BibtexAbstractValue value) {
		this.key = key;
		this.value = value;
	}

	public void accept(final BibtexVisitor visitor) {
		visitor.visit(this);
		this.getValue().accept(visitor);
	}

	/**
	 * @return String
	 */
	public String getKey() {
		return this.key;
	}

	/**
	 * @return BibtexValue
	 */
	public BibtexAbstractValue getValue() {
		return this.value;
	}

	/* (non-Javadoc)
	 * @see bibtex.dom.BibtexNode#printBibtex(java.io.PrintWriter)
	 */
	public void print(final PrintWriter writer) {
		writer.print("@string{");
		writer.print(this.key);
		writer.print("=");

		// Yann 2008/11/01: Bibtex2HTML
		// It seems that Bibtex2HTML really expects the value of a string definition
		// to be enclosed between quotes, not between braces... so I adjust the output.
		final StringWriter tempWriter = new StringWriter();
		this.value.print(new PrintWriter(tempWriter));
		final String beginning = tempWriter.toString().substring(1);
		final String end = beginning.substring(0, beginning.length() - 1);

		writer.print('\"');
		writer.print(end);
		writer.print('\"');
		writer.println("}");

	}

	/**
	 * Sets the key.
	 * @param key The key to set
	 */
	public void setKey(final String key) {
		this.key = key;
	}

	/**
	 * Sets the value.
	 * @param value The value to set
	 */
	public void setValue(final BibtexAbstractValue value) {
		this.value = value;
	}
}
