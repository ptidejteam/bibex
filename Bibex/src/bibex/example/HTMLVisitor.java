/*
 * (c) Copyright 2001-2003 Yann-Ga�l Gu�h�neuc,
 * �cole des Mines de Nantes and Object Technology International, Inc.
 * 
 * Use and copying of this software and preparation of derivative works
 * based upon this software are permitted. Any copy of this software or
 * of any derivative work must include the above copyright notice of
 * the author, this paragraph and the one after it.
 * 
 * This software is made available AS IS, and THE AUTHOR DISCLAIMS
 * ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, AND NOT WITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN,
 * ANY LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF THE AUTHOR IS ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 * 
 * All Rights Reserved.
 */
package bibex.example;

import java.io.PrintWriter;
import bibex.dom.BibtexConcatenatedValue;
import bibex.dom.BibtexEntry;
import bibex.dom.BibtexField;
import bibex.dom.BibtexFile;
import bibex.dom.BibtexKey;
import bibex.dom.BibtexPreamble;
import bibex.dom.BibtexString;
import bibex.dom.BibtexStringDefinition;
import bibex.dom.BibtexStringReference;
import bibex.dom.BibtexToplevelComment;

/**
 * @author	Yann-Ga�l Gu�h�neuc
 */
public class HTMLVisitor implements bibex.visitor.BibtexVisitor {
	private static String computeList(
		final String authors,
		final String language) {

		String output = authors;
		final int lastAnd = authors.lastIndexOf(" and ");
		if (lastAnd > -1) {
			output = output.substring(0, lastAnd);
			output = output.replaceAll(" and ", ", ");
			output = output + authors.substring(lastAnd);
		}
		output =
			output
				.replaceAll(" and ", ' ' + HTMLVisitor.getAnd(language) + ' ');
		return output;
	}

	private static String convertLaTeXtoHTML(final String string) {
		String output = string.replaceAll("\\&", "&amp;");

		output = output.replaceAll("\\�", "&quot;"); // ``
		output = output.replaceAll("\\�", "&quot;"); // ''
		output = output.replaceAll("\\�", "..."); // ...
		output = output.replaceAll("\\�", "&ndash;"); // --

		output = output.replaceAll("\\x5C`A", "&Agrave;");
		output = output.replaceAll("\\x5C'A", "&Aacute;");
		output = output.replaceAll("\\x5C^A", "&Acirc;");
		output = output.replaceAll("\\x5C~A", "&Atilde;");
		output = output.replaceAll("\\x5C\"A", "&Auml;");

		output = output.replaceAll("\\\\c c", "&Ccedil;");
		output = output.replaceAll("\\\\c\\{c\\}", "&Ccedil;");

		output = output.replaceAll("\\x5C`E", "&Egrave;");
		output = output.replaceAll("\\x5C'E", "&Eacute;");
		output = output.replaceAll("\\x5C^E", "&Ecirc;");
		output = output.replaceAll("\\x5C\"E", "&Euml;");

		output = output.replaceAll("\\x5C`I", "&Igrave;");
		output = output.replaceAll("\\x5C'I", "&Iacute;");
		output = output.replaceAll("\\x5C^I", "&Icirc;");
		output = output.replaceAll("\\x5C\"I", "&Iuml;");

		output = output.replaceAll("\\x5C~N", "&Ntilde;");

		output = output.replaceAll("\\x5C`O", "&Ograve;");
		output = output.replaceAll("\\x5C'O", "&Oacute;");
		output = output.replaceAll("\\x5C^O", "&Ocirc;");
		output = output.replaceAll("\\x5C~O", "&Otilde;");
		output = output.replaceAll("\\x5C\"O", "&Ouml;");

		output = output.replaceAll("\\x5C`U", "&Ugrave;");
		output = output.replaceAll("\\x5C'U", "&Uacute;");
		output = output.replaceAll("\\x5C^U", "&Ucirc;");
		output = output.replaceAll("\\x5C\"U", "&Uuml;");

		output = output.replaceAll("\\x5C`a", "&agrave;");
		output = output.replaceAll("\\x5C'a", "&aacute;");
		output = output.replaceAll("\\x5C^a", "&acirc;");
		output = output.replaceAll("\\x5C~a", "&atilde;");
		output = output.replaceAll("\\x5C\"a", "&auml;");

		output = output.replaceAll("\\\\c c", "&ccedil;");
		output = output.replaceAll("\\\\c\\{c\\}", "&ccedil;");

		output = output.replaceAll("\\x5C`e", "&egrave;");
		output = output.replaceAll("\\x5C'e", "&eacute;");
		output = output.replaceAll("\\x5C^e", "&ecirc;");
		output = output.replaceAll("\\x5C\"e", "&euml;");

		output = output.replaceAll("\\x5C`\\{\\\\i\\}", "&igrave;");
		output = output.replaceAll("\\x5C'\\{\\\\i\\}", "&iacute;");
		output = output.replaceAll("\\x5C^\\{\\\\i\\}", "&icirc;");
		output = output.replaceAll("\\x5C\"\\{\\\\i\\}", "&iuml;");

		output = output.replaceAll("\\x5C~n", "&ntilde;");

		output = output.replaceAll("\\x5C`o", "&ograve;");
		output = output.replaceAll("\\x5C'O", "&Oacute;");
		output = output.replaceAll("\\x5C^o", "&ocirc;");
		output = output.replaceAll("\\x5C~o", "&otilde;");
		output = output.replaceAll("\\x5C\"o", "&ouml;");

		output = output.replaceAll("\\x5C`u", "&ugrave;");
		output = output.replaceAll("\\x5C'u", "&uacute;");
		output = output.replaceAll("\\x5C^u", "&ucirc;");
		output = output.replaceAll("\\x5C\"u", "&uuml;");

		output = output.replaceAll("\\$\\\\mu\\$", "&mu;");
		output = output.replaceAll("\\$\\\\sim\\$", "~");

		output = output.replaceAll("\\$\\^\\{st\\}\\$", "<sup>st</sup>");
		output = output.replaceAll("\\$\\^\\{th\\}\\$", "<sup>th</sup>");
		output = output.replaceAll("\\$\\^\\{e\\}\\$", "<sup>e</sup>");

		output = output.replaceAll("\\x5C.*\\{", "");
		output = output.replaceAll("\\{", "");
		output = output.replaceAll("\\}", "");

		output = output.replaceAll("\\x5C", "");

		output = output.replaceAll("``", "'");
		output = output.replaceAll("\"", "'");

		output = output.replaceAll("\\~--", "&nbsp;&ndash;");
		output = output.replaceAll("--", "&ndash;");

		return output;
	}
	private static String getAnd(final String language) {
		if (language.equals("francais")) {
			return "et";
		}
		return "and";
	}
	private static String getEditor(final String language) {
		if (language.equals("francais")) {
			return "�diteur";
		}
		return "editor";
	}
	private static String getEditors(final String language) {
		if (language.equals("francais")) {
			return "�diteurs";
		}
		return "editors";
	}
	private static String getIn(final String language) {
		if (language.equals("francais")) {
			return "";
		}
		return "In";
	}
	private static String getPhDThesis(final String language) {
		if (language.equals("francais")) {
			return "Th�se de doctorat";
		}
		return "Ph.D. thesis";
	}
	private final PrintWriter writer;
	private String currentKey;
	private String author;
	private String title;
	private String editor;

	private String booktitle;
	private String month;
	private String year;
	private String url;
	private String summary;
	private String language;

	public HTMLVisitor(final PrintWriter writer) {
		this.writer = writer;
	}
	public void close(final BibtexEntry bibtexEntry) {
		this.writer.println("\tprint(\"<br>\");");
		this.writer.println("\tbeginTable();");

		this.writer.println("\tprint(\"<tr>\");");
		this.writer.print("\t\taddContentCell(\"");
		this.writer.print(HTMLVisitor.computeList(HTMLVisitor
			.convertLaTeXtoHTML(this.author), this.language));
		this.writer.print(" ; <i>");
		this.writer.print(HTMLVisitor.convertLaTeXtoHTML(this.title));
		this.writer.print("</i> ; ");
		if (bibtexEntry.getType().equals("phdthesis")) {
			this.writer.print(HTMLVisitor.getPhDThesis(this.language));
			this.writer.print(", ");
		}
		else {
			if (this.editor.length() > 0 && this.booktitle.length() > 0) {
				this.writer.print(HTMLVisitor.getIn(this.language));
				this.writer.print(' ');
			}
			if (this.editor.length() > 0) {
				this.writer.print(HTMLVisitor.computeList(HTMLVisitor
					.convertLaTeXtoHTML(this.editor), this.language));
				this.writer.print(", ");
				final String and =
					' ' + HTMLVisitor.getAnd(this.language) + ' ';
				if (this.editor.indexOf(and) > -1) {
					this.writer.print(HTMLVisitor.getEditors(this.language));
				}
				else {
					this.writer.print(HTMLVisitor.getEditor(this.language));
				}
				this.writer.print(", ");
			}
			if (this.booktitle.length() > 0) {
				this.writer.print(HTMLVisitor
					.convertLaTeXtoHTML(this.booktitle));
				this.writer.print(", ");
			}
		}
		this.writer.print(HTMLVisitor.convertLaTeXtoHTML(this.month));
		this.writer.print(' ');
		this.writer.print(HTMLVisitor.convertLaTeXtoHTML(this.year));
		this.writer.print("\");");
		this.writer.println("\n\tprint(\"</tr>\");");

		if (this.url.length() > 0) {
			final String url =
				HTMLVisitor.convertLaTeXtoHTML(this.url.replaceAll(" ", ""));
			this.writer.println("\tprint(\"<tr>\");");
			this.writer.print("\t\taddContentCell(\"<a href=\\\"http://");
			this.writer.print(url);
			this.writer.print("\\\">");
			this.writer.print(url);
			this.writer.print("</a>\");");
			this.writer.println("\n\tprint(\"</tr>\");");
		}

		if (this.summary.length() > 0) {
			this.writer.println("\tprint(\"<tr>\");");
			this.writer.print("\t\taddContentCell(\"");
			this.writer.print(HTMLVisitor.convertLaTeXtoHTML(this.summary));
			this.writer.print("\");");
			this.writer.println("\n\tprint(\"</tr>\");");
		}

		this.writer.println("\tendTable();");
	}
	public void close(final BibtexField bibtexField) {
	}
	public void close(final BibtexFile bibtexFile) {
		this.writer
			.println("\tend_body_with_title_stats($home_name, $home_url, $previous_name, $previous_url, $next_name, $next_url, \"AC8ZfQpeispGXVdXORx3U08VVCZQ\");");
		this.writer.println("?>");
		this.writer.flush();
	}
	public void open(final BibtexEntry bibtexEntry) {
		this.reset();
	}
	public void open(final BibtexField bibtexField) {
	}
	public void open(final BibtexFile bibtexFile) {
		this.writer.println("<?php");
		this.writer.println("\trequire(\"./$language.txt\");");
		this.writer.println("\tdocument(\"\", $chunk[\"Title\"]);");
		this.writer.println("\tbegin_body();");
		this.writer
			.println("\ttitle($home_name, $home_url, $previous_name, $previous_url, $next_name, $next_url, $chunk[\"Title\"]);");
		this.writer.println("\tparagraph($chunk[\"Paragraph 1\"]);");
		this.writer.println("\tparagraph($chunk[\"Paragraph 2\"]);");
		this.writer.println("\tparagraph($chunk[\"Paragraph 3\"]);");
	}
	private void reset() {
		this.author = "";
		this.title = "";
		this.editor = "";
		this.booktitle = "";
		this.month = "";
		this.year = "";
		this.url = "";
		this.summary = "";
		this.language = "";
	}
	public void visit(final BibtexConcatenatedValue bibtexConcatenatedValue) {
		this.writer.println(bibtexConcatenatedValue.getLeft());
		this.writer.println(bibtexConcatenatedValue.getRight());
	}
	public void visit(final BibtexKey bibtexKey) {
		this.currentKey = bibtexKey.getValue();
	}
	public void visit(final BibtexPreamble bibtexPreamble) {
	}
	public void visit(final BibtexString bibtexString) {
		if (this.currentKey.equals("author")) {
			this.author = bibtexString.getValue();
		}
		else if (this.currentKey.equals("title")) {
			this.title = bibtexString.getValue();
		}
		else if (this.currentKey.equals("editor")) {
			this.editor = bibtexString.getValue();
		}
		else if (this.currentKey.equals("booktitle")) {
			this.booktitle = bibtexString.getValue();
		}
		else if (this.currentKey.equals("journal")) {
			this.booktitle = bibtexString.getValue();
		}
		else if (this.currentKey.equals("publisher")) {
			bibtexString.getValue();
		}
		else if (this.currentKey.equals("month")) {
			this.month = bibtexString.getValue();
		}
		else if (this.currentKey.equals("year")) {
			this.year = bibtexString.getValue();
		}
		else if (this.currentKey.equals("url")) {
			this.url = bibtexString.getValue();
		}
		else if (this.currentKey.equals("abstract")) {
			this.summary = bibtexString.getValue();
		}
		else if (this.currentKey.equals("language")) {
			this.language = bibtexString.getValue();
		}
	}
	public void visit(final BibtexStringDefinition bibtexStringDefinition) {
	}
	public void visit(final BibtexStringReference bibtexStringReference) {
	}
	public void visit(final BibtexToplevelComment bibtexToplevelComment) {
	}
}
