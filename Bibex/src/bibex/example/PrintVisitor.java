/*
 * (c) Copyright 2001-2003 Yann-Ga�l Gu�h�neuc,
 * �cole des Mines de Nantes and Object Technology International, Inc.
 * 
 * Use and copying of this software and preparation of derivative works
 * based upon this software are permitted. Any copy of this software or
 * of any derivative work must include the above copyright notice of
 * the author, this paragraph and the one after it.
 * 
 * This software is made available AS IS, and THE AUTHOR DISCLAIMS
 * ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, AND NOT WITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN,
 * ANY LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF THE AUTHOR IS ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 * 
 * All Rights Reserved.
 */
package bibex.example;

import java.io.PrintWriter;
import bibex.dom.BibtexConcatenatedValue;
import bibex.dom.BibtexEntry;
import bibex.dom.BibtexField;
import bibex.dom.BibtexFile;
import bibex.dom.BibtexKey;
import bibex.dom.BibtexPreamble;
import bibex.dom.BibtexString;
import bibex.dom.BibtexStringDefinition;
import bibex.dom.BibtexStringReference;
import bibex.dom.BibtexToplevelComment;
import bibex.visitor.BibtexVisitor;

/**
 * @author	Yann-Ga�l Gu�h�neuc
 */
public class PrintVisitor implements BibtexVisitor {
	private final PrintWriter writer;

	public PrintVisitor(final PrintWriter writer) {
		this.writer = writer;
	}
	public void close(final BibtexEntry bibtexEntry) {
		this.writer.println("}");
	}
	public void close(final BibtexField bibtexField) {
	}
	public void close(final BibtexFile bibtexFile) {
	}
	public void open(final BibtexEntry bibtexEntry) {
		this.writer.print('@');
		this.writer.print(bibtexEntry.getType());
		this.writer.print('{');
		this.writer.print(bibtexEntry.getKey());
		this.writer.println(',');
	}
	public void open(final BibtexField bibtexField) {
	}
	public void open(final BibtexFile bibtexFile) {
	}
	public void visit(final BibtexConcatenatedValue bibtexConcatenatedValue) {
		this.writer.println(bibtexConcatenatedValue.getLeft());
		this.writer.println(bibtexConcatenatedValue.getRight());
	}
	public void visit(final BibtexKey bibtexKey) {
		this.writer.print('\t');
		this.writer.print(bibtexKey.getValue());
		this.writer.print("=");
	}
	public void visit(final BibtexPreamble bibtexPreamble) {
		this.writer.println(bibtexPreamble.getContent());
	}
	public void visit(final BibtexString bibtexString) {
		this.writer.print('{');
		this.writer.print(bibtexString.getValue());
		this.writer.println("},");
	}
	public void visit(final BibtexStringDefinition bibtexStringDefinition) {
		this.writer.print('\n');
		this.writer.print(bibtexStringDefinition.getKey());
		this.writer.print("={");
		this.writer.print(bibtexStringDefinition.getValue());
		this.writer.println("}");
	}
	public void visit(final BibtexStringReference bibtexStringReference) {
		this.writer.println(bibtexStringReference.getValue());
	}
	public void visit(final BibtexToplevelComment bibtexToplevelComment) {
		this.writer.println(bibtexToplevelComment.getContent());
	}
}
