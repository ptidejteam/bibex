/*
 * Created on Mar 21, 2003
 *
 * @author henkel@cs.colorado.edu
 * 
 */
package bibex.example;

import java.io.FileReader;
import java.io.PrintWriter;
import bibex.dom.BibtexFile;
import bibex.dom.util.Expansions;
import bibex.dom.util.Expansions.ExpansionException;
import bibex.parser.BibtexParser;
import bibex.parser.ParseException;

/**
 * @author henkel
 */
public final class Parse {

	public static void main(final String[] args) {
		if (args.length < 1) {
			Parse.usage();
			return;
		}
		final BibtexFile bibtexFile = new BibtexFile();
		final BibtexParser parser = new BibtexParser();
		boolean expandStrings = false;
		boolean dropStrings = false;
		boolean expandCrossrefs = false;
		for (int argsIndex = 0; argsIndex < args.length - 1; argsIndex++) {
			final String argument = args[argsIndex];
			if (argument.equals("-expandStringDefinitions")) {
				expandStrings = true;
			}
			else if (argument.equals("-expandAndDropStringDefinitions")) {
				expandStrings = dropStrings = true;
			}
			else if (argument.equals("-expandCrossReferences")) {
				expandCrossrefs = expandStrings = true;
			}
			else {
				System.err.println("Illegal argument: " + argument);
				Parse.usage();
			}
		}

		try {
			final String filename = args[args.length - 1];
			System.err.println("Parsing \"" + filename + "\" ... ");
			parser.parse(
				bibtexFile,
				new FileReader(args[args.length - 1]),
				false);
		}
		catch (final Exception e) {
			System.err.println("Fatal exception: ");
			e.printStackTrace();
			return;
		}
		finally {
			final ParseException[] exceptions = parser.getExceptions();
			if (exceptions.length > 0) {
				System.err.println("Non-fatal exceptions: ");
				for (int i = 0; i < exceptions.length; i++) {
					exceptions[i].printStackTrace();
					System.err.println("===================");
				}
			}
		}
		try {
			if (expandStrings) {
				System.err.println("\n\nExpanding strings ...");
				Expansions.expandStringReferences(bibtexFile, dropStrings);
			}
			if (expandCrossrefs) {
				System.err.println("\n\nExpanding crossrefs ...");
				Expansions.expandCrossReferences(bibtexFile);
			}
		}
		catch (final ExpansionException e1) {
			e1.printStackTrace();
			return;
		}
		System.err.println("\n\nGenerating output ...");
		final PrintWriter out = new PrintWriter(System.out);
		bibtexFile.print(out);
		out.flush();
	}

	public static void usage() {
		System.err
			.println("\nUsage: bibtex.example.Parse [-expandStringDefinitions]\n"
					+ "         [-expandAndDropStringDefinitions] [-expandCrossReferences] <file.bib>");
		System.err
			.println("\nNote: Selecting -expandCrossReferences implies that we will ");
		System.err
			.println("      expand the string definitions as well (for consistency).");
		System.err
			.println("\nThe output will be given on stdout, errors and messages will be printed to stderr.\n\n");
	}
}
