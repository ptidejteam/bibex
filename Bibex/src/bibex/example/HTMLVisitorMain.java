/*
 * (c) Copyright 2001-2003 Yann-Ga�l Gu�h�neuc,
 * �cole des Mines de Nantes and Object Technology International, Inc.
 * 
 * Use and copying of this software and preparation of derivative works
 * based upon this software are permitted. Any copy of this software or
 * of any derivative work must include the above copyright notice of
 * the author, this paragraph and the one after it.
 * 
 * This software is made available AS IS, and THE AUTHOR DISCLAIMS
 * ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, AND NOT WITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN,
 * ANY LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF THE AUTHOR IS ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 * 
 * All Rights Reserved.
 */
package bibex.example;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import bibex.dom.BibtexFile;
import bibex.parser.BibtexParser;
import bibex.parser.ParseException;

/**
 * @author	Yann-Ga�l Gu�h�neuc
 */
public class HTMLVisitorMain {
	public static void main(final String[] args) {
		try {
			final BibtexFile bibtexFile = new BibtexFile();
			final BibtexParser bibtexParser = new BibtexParser();
			final PrintWriter writer =
				new PrintWriter(
					new FileWriter(
						"C:/Documents and Settings/WEB Site/yann-gael/Work/Research/BibTeX/Content.php"));

			bibtexParser
				.parse(
					bibtexFile,
					new FileReader(
						"C:/Documents and Settings/Yann/Mes documents/Common/Read.bib"),
					true);
			// bibtexFile.accept(new PrintVisitor(new PrintWriter(System.out)));
			bibtexFile.accept(new HTMLVisitor(writer));
		}
		catch (final FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (final ParseException e) {
			e.printStackTrace();
		}
		catch (final IOException e) {
			e.printStackTrace();
		}
	}
}