/*
 * (c) Copyright 2001-2003 Yann-Ga�l Gu�h�neuc,
 * �cole des Mines de Nantes and Object Technology International, Inc.
 * 
 * Use and copying of this software and preparation of derivative works
 * based upon this software are permitted. Any copy of this software or
 * of any derivative work must include the above copyright notice of
 * the author, this paragraph and the one after it.
 * 
 * This software is made available AS IS, and THE AUTHOR DISCLAIMS
 * ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, AND NOT WITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN,
 * ANY LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF THE AUTHOR IS ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 * 
 * All Rights Reserved.
 */
package bibex.extractor.ui;

import java.awt.Adjustable;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.prefs.Preferences;
import javax.swing.BoundedRangeModel;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultBoundedRangeModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileFilter;
import bibex.extractor.BibtexExtractor;
import bibex.extractor.ExtractorFunction;
import bibex.parser.ParseException;
import com.thoughtworks.xstream.XStream;

/**
 * @author	Yann-Ga�l Gu�h�neuc
 */
public class Bibex extends JFrame {
	private static final FileFilter BIBEX_BUNDLE_OBJECT_FILTER =
		new FileFilter() {
			public boolean accept(final File file) {
				return file.getName().endsWith(".bibex.bundle")
						|| file.isDirectory();
			}

			public String getDescription() {
				return "Bibex bundle of configurations (object file)";
			}
		};
	private static final FileFilter BIBEX_BUNDLE_XML_FILTER = new FileFilter() {
		public boolean accept(final File file) {
			return file.getName().endsWith(".bibex.bundle.xml")
					|| file.isDirectory();
		}

		public String getDescription() {
			return "Bibex bundle of configurations (XML file)";
		}
	};
	private static final FileFilter BIBEX_FILE_FILTER = new FileFilter() {
		public boolean accept(final File file) {
			return file.getName().endsWith(".bibex") || file.isDirectory();
		}
		public String getDescription() {
			return "Bibex configuration";
		}
	};
	private static final String BIBTEX_EXTRACTOR_NAME = "Bibex";
	private static final String BIBTEX_EXTRACTOR_VERSION = "2.0.4";
	private static final FileFilter BIBTEX_FILE_FILTER = new FileFilter() {
		public boolean accept(final File file) {
			return file.getName().endsWith(".bib") || file.isDirectory();
		}
		public String getDescription() {
			return "BibTeX bases";
		}
	};
	private static final String PREFS_KEY_BUNDLE_FULL_PATH = "Bundle Full Path";
	private static final String PREFS_KEY_WINDOW_X_POSITION =
		"Window X Position";
	private static final String PREFS_KEY_WINDOW_Y_POSITION =
		"Window Y Position";
	private static final RegexWindow REGEX_WINDOW = RegexWindow
		.getUniqueInstance();
	private static final long serialVersionUID = 7066792035653307923L;
	private static final int WARNING_DURATION = 2;
	private static final Dimension WINDOW_DIMENSION = new Dimension(600, 300);

	public static void main(final String[] args) {
		final Bibex ui = new Bibex();
		ui.setVisible(true);
	}

	private JList basesList;
	private String bundleFullPath = "";
	private final BoundedRangeModel bundleModel = new DefaultBoundedRangeModel(
		0,
		1,
		0,
		1);
	private ExtractionDataSet currentExtractionDataSet;
	private final List extractionDataSets = new ArrayList(3);
	private final JTextField infoField;
	private int messageDuration;
	private final MouseAdapter mouseAdapter = new MouseAdapter() {
		public void mouseEntered(final MouseEvent mouseEvent) {
			Bibex.this.informUser(Bibex.this
				.helpOnComponents((JComponent) mouseEvent.getComponent()));
		}
		public void mouseExited(final MouseEvent mouseEvent) {
			Bibex.this.informUser("");
		}
	};
	private JTextField newBibtexFileField;
	private JList regexesList;
	private int windowXPosition;
	private int windowYPosition;

	private Bibex() {
		this.start();

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		this.getContentPane().setLayout(
			new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		this.setLocation(this.windowXPosition, this.windowYPosition);
		this.setResizable(false);
		this.setSize(Bibex.WINDOW_DIMENSION);

		this.bundleModel.addChangeListener(new ChangeListener() {
			public void stateChanged(final ChangeEvent e) {
				Bibex.this.currentExtractionDataSet =
					(ExtractionDataSet) Bibex.this.extractionDataSets
						.get(((BoundedRangeModel) e.getSource()).getValue());
				Bibex.this.updateDisplay();
			}
		});
		this.currentExtractionDataSet = new ExtractionDataSet();
		this.extractionDataSets.add(this.currentExtractionDataSet);

		this.resetConfiguration();

		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(final WindowEvent windowEvent) {
				Bibex.this.stop();
			}
		});

		// Yann 2003/07/05: Ugly!
		// Is there anyway to remove the declaration of
		// the infoField *outside* of this constructor,
		// not to pollute the class declaration?
		this.infoField = new JTextField();
		this.infoField.setEditable(false);

		this.createMenus();
		this.createButtons();
		this.about();

		Bibex.REGEX_WINDOW.addRegexWindowListener(new RegexWindowListener() {
			public void regexAdded(final Regex regex) {
				if (Bibex.this.currentExtractionDataSet.getRegexesSet().add(
					regex)) {
					final Object[] regexes =
						Bibex.this.currentExtractionDataSet
							.getRegexesSet()
							.toArray();
					Arrays.sort(regexes);
					Bibex.this.regexesList.setListData(regexes);
					Bibex.this.informUser("Regex added successfully");
				}
				else {
					Bibex.this.warnUser("Regex already exists");
				}
			}
		});
	}
	private void about() {
		this.warnUser(Bibex.BIBTEX_EXTRACTOR_NAME
				+ " (a.k.a. WinBibEX) � Yann-Ga�l Gu�h�neuc, version "
				+ Bibex.BIBTEX_EXTRACTOR_VERSION);
	}
	private void addBase() {
		final JFileChooser fileChooser =
			new JFileChooser(this.currentExtractionDataSet.getCurrentPath());
		fileChooser.setFileFilter(Bibex.BIBTEX_FILE_FILTER);
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooser.setMultiSelectionEnabled(true);
		if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			final File[] selectedFiles = fileChooser.getSelectedFiles();
			boolean isEverythingAllRight = true;
			for (int i = 0; i < selectedFiles.length; i++) {
				try {
					this.currentExtractionDataSet
						.setCurrentPath(selectedFiles[i].getPath());

					isEverythingAllRight &=
						this.currentExtractionDataSet.getBasesSet().add(
							new Base(
								selectedFiles[i].getName(),
								selectedFiles[i].getCanonicalPath()));
				}
				catch (final FileNotFoundException fnfe) {
					this.warnUser(fnfe.getMessage());
				}
				catch (final ParseException pe) {
					this.warnUser(pe.getMessage());
				}
				catch (final IOException ioe) {
					this.warnUser(ioe.getMessage());
				}
			}
			if (isEverythingAllRight) {
				final Object[] bibtexFiles =
					this.currentExtractionDataSet.getBasesSet().toArray();
				Arrays.sort(bibtexFiles);
				this.basesList.setListData(bibtexFiles);
				this.cacheBaseData();
				this.informUser("Base added successfully");
			}
			else {
				this.warnUser("Cannot add twice the same base");
			}
		}
	}
	private void addConfiguration() {
		this.currentExtractionDataSet = new ExtractionDataSet();
		this.extractionDataSets.add(this.currentExtractionDataSet);

		this.bundleModel.setMaximum(this.bundleModel.getMaximum() + 1);
		this.bundleModel.setValue(this.bundleModel.getMaximum());
	}
	private void addRegex() {
		if (this.currentExtractionDataSet.getBasesSet().size() == 0) {
			this
				.warnUser("At least one BibTeX base must be present to add regex");
		}
		else {
			Bibex.REGEX_WINDOW.setVisible(true);
		}
	}
	private void browse() {
		final JFileChooser fileChooser =
			new JFileChooser(this.currentExtractionDataSet.getCurrentPath());
		fileChooser.setFileFilter(Bibex.BIBTEX_FILE_FILTER);
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooser.setMultiSelectionEnabled(false);
		if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {

			try {
				this.currentExtractionDataSet.setCurrentPath(fileChooser
					.getSelectedFile()
					.getPath());

				final String file =
					fileChooser.getSelectedFile().getCanonicalPath();
				if (file.endsWith(".bib")) {
					this.currentExtractionDataSet.setNewBibtexFile(file);
				}
				else {
					this.currentExtractionDataSet.setNewBibtexFile(file
							+ ".bib");
				}
				this.newBibtexFileField.setText(this.currentExtractionDataSet
					.getNewBibtexFile());
			}
			catch (final IOException ioe) {
				this.warnUser(ioe.getMessage());
			}
		}
	}
	private void cacheBaseData() {
		// Yann 2004/07/17: Listener!
		// The updating of the display when caching the base
		// should be performed through a callback using a
		// listener (Main listening to ExtractionDataSet).
		this.informUser("Caching fields from BibTeX bases...");
		this.currentExtractionDataSet.cacheBaseData();
		Bibex.REGEX_WINDOW.setFields(this.currentExtractionDataSet
			.getFieldsSet());
		Bibex.REGEX_WINDOW.setKeys(this.currentExtractionDataSet.getKeysSet());
		Bibex.REGEX_WINDOW
			.setTypes(this.currentExtractionDataSet.getTypesSet());
		this.informUser("Caching done");
	}
	private void createButtons() {
		final JPanel bundleTitlePanel = new JPanel();
		bundleTitlePanel.setBorder(new EmptyBorder(5, 1, 5, 5));
		bundleTitlePanel.setLayout(new BorderLayout());
		final JPanel bundlePanel = new JPanel();
		bundlePanel.setLayout(new BoxLayout(bundlePanel, BoxLayout.X_AXIS));
		final JPanel configurationTitlePanel = new JPanel();
		configurationTitlePanel.setBorder(new EmptyBorder(5, 1, 5, 5));
		configurationTitlePanel.setLayout(new BorderLayout());
		final JPanel configurationPanel = new JPanel();
		configurationPanel.setLayout(new BoxLayout(
			configurationPanel,
			BoxLayout.X_AXIS));
		// this.getContentPane().add(new JSeparator(SwingConstants.HORIZONTAL));
		this.getContentPane().add(bundleTitlePanel);
		this.getContentPane().add(bundlePanel);
		//	this.getContentPane().add(new JLabel(new Icon() {
		//		public int getIconHeight() {
		//			return 5;
		//		}
		//		public int getIconWidth() {
		//			return 0;
		//		}
		//		public void paintIcon(
		//			final Component c,
		//			final Graphics g,
		//			final int x,
		//			final int y) {
		//		}
		//	}));
		this.getContentPane().add(new JSeparator(SwingConstants.HORIZONTAL));
		//	this.getContentPane().add(new JLabel(new Icon() {
		//		public int getIconHeight() {
		//			return 5;
		//		}
		//		public int getIconWidth() {
		//			return 0;
		//		}
		//		public void paintIcon(
		//			final Component c,
		//			final Graphics g,
		//			final int x,
		//			final int y) {
		//		}
		//	}));
		this.getContentPane().add(configurationTitlePanel);
		this.getContentPane().add(configurationPanel);
		this.getContentPane().add(this.infoField);

		// +--------+
		// | Titles |
		// +--------+

		final JLabel bundleTitleLabel = new JLabel("Bundle:");
		bundleTitleLabel.setForeground(Color.BLUE);
		bundleTitlePanel.add(bundleTitleLabel, BorderLayout.WEST);

		final JLabel configurationTitleLabel = new JLabel("Configuration:");
		configurationTitleLabel.setForeground(Color.BLUE);
		configurationTitlePanel.add(configurationTitleLabel, BorderLayout.WEST);

		// +--------------+
		// | Bundle panel |
		// +--------------+

		final JButton addConfigurationButton = new JButton("Add");
		addConfigurationButton.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent actionEvent) {
				Bibex.this.addConfiguration();
			}
		});
		addConfigurationButton.addMouseListener(this.mouseAdapter);
		addConfigurationButton.setToolTipText("Add a new configuration");
		bundlePanel.add(addConfigurationButton);

		final JButton removeConfigurationButton = new JButton("Remove");
		removeConfigurationButton.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent actionEvent) {
				Bibex.this.removeConfiguration();
			}
		});
		removeConfigurationButton.addMouseListener(this.mouseAdapter);
		removeConfigurationButton.setToolTipText("Remove this configuration");
		bundlePanel.add(removeConfigurationButton);

		final JScrollBar configurationScrollbar =
			new JScrollBar(Adjustable.HORIZONTAL);
		configurationScrollbar.setModel(this.bundleModel);
		configurationScrollbar.addMouseListener(this.mouseAdapter);
		configurationScrollbar.setToolTipText("Move through configurations");
		bundlePanel.add(configurationScrollbar);

		final JButton previewAllButton = new JButton("Preview all!");
		previewAllButton.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent actionEvent) {
				Bibex.this.previewAll();
			}
		});
		previewAllButton.addMouseListener(this.mouseAdapter);
		previewAllButton
			.setToolTipText("Preview extraction of all configurations");
		bundlePanel.add(previewAllButton);

		final JButton extractAllButton = new JButton("Extract all!");
		extractAllButton.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent actionEvent) {
				Bibex.this.extractAll();
			}
		});
		extractAllButton.addMouseListener(this.mouseAdapter);
		extractAllButton
			.setToolTipText("Extract entries of all configurations");
		bundlePanel.add(extractAllButton);

		// +--------------+
		// | Origin panel |
		// +--------------+

		final JPanel originPanel = new JPanel();
		originPanel.setLayout(new GridBagLayout());
		configurationPanel.add(originPanel);

		final JLabel bibtexFileLabel = new JLabel("Origin BibTeX bases");
		originPanel.add(bibtexFileLabel, new GridBagConstraints(
			0,
			0,
			1,
			1,
			1.0,
			0.0,
			GridBagConstraints.CENTER,
			GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0),
			0,
			0));

		this.basesList = new JList();
		this.basesList.addMouseListener(this.mouseAdapter);
		this.basesList
			.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		this.basesList
			.setToolTipText("List of BibTeX bases to extract entries from");
		final JScrollPane bibtexFilesScrollPane =
			new JScrollPane(this.basesList);
		originPanel.add(bibtexFilesScrollPane, new GridBagConstraints(
			0,
			1,
			1,
			1,
			1.0,
			1.0,
			GridBagConstraints.CENTER,
			GridBagConstraints.BOTH,
			new Insets(0, 0, 0, 0),
			0,
			0));

		final JButton addBibtexFileButton = new JButton("Add");
		addBibtexFileButton.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent actionEvent) {
				Bibex.this.addBase();
			}
		});
		addBibtexFileButton.addMouseListener(this.mouseAdapter);
		addBibtexFileButton
			.setToolTipText("Add a BibTeX base to extract entries from");
		originPanel.add(addBibtexFileButton, new GridBagConstraints(
			0,
			2,
			1,
			1,
			1.0,
			0.0,
			GridBagConstraints.CENTER,
			GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0),
			0,
			0));

		final JButton removeBibtexFileButton = new JButton("Remove");
		removeBibtexFileButton.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent actionEvent) {
				Bibex.this.removeBase();
			}
		});
		removeBibtexFileButton.addMouseListener(this.mouseAdapter);
		removeBibtexFileButton
			.setToolTipText("Remove the selected BibTeX base from the list");
		originPanel.add(removeBibtexFileButton, new GridBagConstraints(
			0,
			3,
			1,
			1,
			1.0,
			0.0,
			GridBagConstraints.CENTER,
			GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0),
			0,
			0));

		// +-------------+
		// | Regex panel |
		// +-------------+

		final JPanel regexPanel = new JPanel();
		regexPanel.setLayout(new GridBagLayout());
		configurationPanel.add(regexPanel);

		final JLabel regexLabel = new JLabel("Regexes");
		regexPanel.add(regexLabel, new GridBagConstraints(
			0,
			0,
			1,
			1,
			1.0,
			0.0,
			GridBagConstraints.CENTER,
			GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0),
			0,
			0));

		this.regexesList = new JList();
		this.regexesList.addMouseListener(this.mouseAdapter);
		this.regexesList
			.setToolTipText("List of regular expression to choose entries with");
		final JScrollPane regexListScrollPane =
			new JScrollPane(this.regexesList);
		regexPanel.add(regexListScrollPane, new GridBagConstraints(
			0,
			1,
			1,
			1,
			1.0,
			1.0,
			GridBagConstraints.CENTER,
			GridBagConstraints.BOTH,
			new Insets(0, 0, 0, 0),
			0,
			0));

		final JButton addRegexButton = new JButton("Add");
		addRegexButton.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent actionEvent) {
				Bibex.this.addRegex();
			}
		});
		addRegexButton.addMouseListener(this.mouseAdapter);
		addRegexButton
			.setToolTipText("Add a new regular expression to choose entries with");
		regexPanel.add(addRegexButton, new GridBagConstraints(
			0,
			2,
			1,
			1,
			1.0,
			0.0,
			GridBagConstraints.CENTER,
			GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0),
			0,
			0));

		final JButton removeRegexButton = new JButton("Remove");
		removeRegexButton.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent actionEvent) {
				Bibex.this.removeRegex();
			}
		});
		removeRegexButton.addMouseListener(this.mouseAdapter);
		removeRegexButton
			.setToolTipText("Remove the selected regular expression from the list");
		regexPanel.add(removeRegexButton, new GridBagConstraints(
			0,
			3,
			1,
			1,
			1.0,
			0.0,
			GridBagConstraints.CENTER,
			GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0),
			0,
			0));

		// +-------------------+
		// | Destination panel |
		// +-------------------+

		final JPanel destinationPanel = new JPanel();
		destinationPanel.setLayout(new GridBagLayout());
		configurationPanel.add(destinationPanel);

		final JLabel newBibtexFileLabel = new JLabel("New BibTeX file");
		newBibtexFileLabel.addFocusListener(new FocusListener() {
			public void focusGained(final FocusEvent docusEvent) {
			}
			public void focusLost(final FocusEvent focusEvent) {
				final JLabel label = (JLabel) focusEvent.getComponent();
				final String file = label.getText();
				if (!file.endsWith(".bib")) {
					label.setText(file + ".bib");
				}
			}
		});
		destinationPanel.add(newBibtexFileLabel, new GridBagConstraints(
			0,
			0,
			1,
			1,
			1.0,
			0.0,
			GridBagConstraints.CENTER,
			GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0),
			0,
			0));

		this.newBibtexFileField = new JTextField(20);
		this.newBibtexFileField.addMouseListener(this.mouseAdapter);
		this.newBibtexFileField
			.setToolTipText("BibTeX base to which extract entries");
		destinationPanel.add(this.newBibtexFileField, new GridBagConstraints(
			0,
			1,
			1,
			1,
			1.0,
			0.0,
			GridBagConstraints.CENTER,
			GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0),
			0,
			0));

		final JButton browseButton = new JButton("Browse");
		browseButton.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent actionEvent) {
				Bibex.this.browse();
			}
		});
		browseButton.addMouseListener(this.mouseAdapter);
		browseButton
			.setToolTipText("Select the BibTeX base to which extract entries");
		destinationPanel.add(browseButton, new GridBagConstraints(
			0,
			2,
			1,
			1,
			1.0,
			0.0,
			GridBagConstraints.CENTER,
			GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0),
			0,
			0));

		final JLabel adLabel = new JLabel("", SwingConstants.CENTER);
		adLabel.setForeground(Color.BLUE);
		destinationPanel.add(adLabel, new GridBagConstraints(
			0,
			3,
			1,
			1,
			1.0,
			1.0,
			GridBagConstraints.CENTER,
			GridBagConstraints.BOTH,
			new Insets(0, 0, 0, 0),
			0,
			0));

		final JButton previewButton = new JButton("Preview");
		previewButton.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent actionEvent) {
				Bibex.this.preview();
			}
		});
		previewButton.addMouseListener(this.mouseAdapter);
		previewButton.setToolTipText("Preview the result of extraction");
		destinationPanel.add(previewButton, new GridBagConstraints(
			0,
			4,
			1,
			1,
			1.0,
			0.0,
			GridBagConstraints.CENTER,
			GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0),
			0,
			0));

		final JButton extractButton = new JButton("Extract it!");
		extractButton.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent actionEvent) {
				Bibex.this.extract();
			}
		});
		extractButton.addMouseListener(this.mouseAdapter);
		extractButton.setToolTipText("Extract entries!");
		destinationPanel.add(extractButton, new GridBagConstraints(
			0,
			5,
			1,
			1,
			1.0,
			0.0,
			GridBagConstraints.CENTER,
			GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 0, 0),
			0,
			0));
	}
	private void createMenus() {
		final JMenuBar menuBar = new JMenuBar();
		this.setJMenuBar(menuBar);
		this.createFileMenus(menuBar);
		this.createBundleMenus(menuBar);
		this.createConfigurationMenus(menuBar);
		menuBar.add(Box.createHorizontalGlue());
		this.createHelpMenus(menuBar);

	}
	private void createFileMenus(final JMenuBar menuBar) {
		final JMenu fileMenu = new JMenu("File");
		menuBar.add(fileMenu);

		final JMenuItem aboutItem = new JMenuItem("About");
		aboutItem.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent actionEvent) {
				Bibex.this.about();
			}
		});
		aboutItem.addMouseListener(this.mouseAdapter);
		aboutItem.setToolTipText("About " + Bibex.BIBTEX_EXTRACTOR_NAME);
		fileMenu.add(aboutItem);

		final JMenuItem quitItem = new JMenuItem("Quit");
		quitItem.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent actionEvent) {
				Bibex.this.stop();
			}
		});
		quitItem.addMouseListener(this.mouseAdapter);
		quitItem.setToolTipText("Quit " + Bibex.BIBTEX_EXTRACTOR_NAME);
		fileMenu.add(quitItem);
	}
	private void createBundleMenus(final JMenuBar menuBar) {
		final JMenu configurationBundleMenu = new JMenu("Bundle");
		menuBar.add(configurationBundleMenu);

		final JMenuItem loadObjectConfigurationBundleItem =
			new JMenuItem("Load from object file");
		loadObjectConfigurationBundleItem
			.addActionListener(new ActionListener() {
				public void actionPerformed(final ActionEvent actionEvent) {
					Bibex.this.loadObjectConfigurationBundle();
				}
			});
		loadObjectConfigurationBundleItem.addMouseListener(this.mouseAdapter);
		loadObjectConfigurationBundleItem
			.setToolTipText("Load a bundle of configurations from an object file");
		configurationBundleMenu.add(loadObjectConfigurationBundleItem);

		final JMenuItem loadXMLConfigurationBundleItem =
			new JMenuItem("Load from XML file");
		loadXMLConfigurationBundleItem.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent actionEvent) {
				Bibex.this.loadXMLConfigurationBundle();
			}
		});
		loadXMLConfigurationBundleItem.addMouseListener(this.mouseAdapter);
		loadXMLConfigurationBundleItem
			.setToolTipText("Load a bundle of configurations from an XML file");
		configurationBundleMenu.add(loadXMLConfigurationBundleItem);

		final JMenuItem saveObjectConfigurationBundleItem =
			new JMenuItem("Save to object file");
		saveObjectConfigurationBundleItem
			.addActionListener(new ActionListener() {
				public void actionPerformed(final ActionEvent actionEvent) {
					Bibex.this.saveObjectConfigurationBundle();
				}
			});
		saveObjectConfigurationBundleItem.addMouseListener(this.mouseAdapter);
		saveObjectConfigurationBundleItem
			.setToolTipText("Save this bundle of configurations to an object file");
		configurationBundleMenu.add(saveObjectConfigurationBundleItem);

		final JMenuItem saveXMLConfigurationBundleItem =
			new JMenuItem("Save to XML file");
		saveXMLConfigurationBundleItem.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent actionEvent) {
				Bibex.this.saveXMLConfigurationBundle();
			}
		});
		saveXMLConfigurationBundleItem.addMouseListener(this.mouseAdapter);
		saveXMLConfigurationBundleItem
			.setToolTipText("Save this bundle of configurations to an object file");
		configurationBundleMenu.add(saveXMLConfigurationBundleItem);

		final JMenuItem saveObjectConfigurationBundleAsItem =
			new JMenuItem("Save as object file...");
		saveObjectConfigurationBundleAsItem
			.addActionListener(new ActionListener() {
				public void actionPerformed(final ActionEvent actionEvent) {
					Bibex.this.saveObjectConfigurationBundleAs();
				}
			});
		saveObjectConfigurationBundleAsItem.addMouseListener(this.mouseAdapter);
		saveObjectConfigurationBundleAsItem
			.setToolTipText("Save this bundle of configurations to an object file with a new name");
		configurationBundleMenu.add(saveObjectConfigurationBundleAsItem);

		final JMenuItem saveXMLConfigurationBundleAsItem =
			new JMenuItem("Save as XML file...");
		saveXMLConfigurationBundleAsItem
			.addActionListener(new ActionListener() {
				public void actionPerformed(final ActionEvent actionEvent) {
					Bibex.this.saveXMLConfigurationBundleAs();
				}
			});
		saveXMLConfigurationBundleAsItem.addMouseListener(this.mouseAdapter);
		saveXMLConfigurationBundleAsItem
			.setToolTipText("Save this bundle of configurations to an XML file with a new name");
		configurationBundleMenu.add(saveXMLConfigurationBundleAsItem);

		configurationBundleMenu.addSeparator();

		final JMenuItem resetConfigurationBundleItem = new JMenuItem("Reset");
		resetConfigurationBundleItem.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent actionEvent) {
				Bibex.this.resetConfigurationBundle();
			}
		});
		resetConfigurationBundleItem.addMouseListener(this.mouseAdapter);
		resetConfigurationBundleItem
			.setToolTipText("Reset this bundle of configurations");
		configurationBundleMenu.add(resetConfigurationBundleItem);
	}
	private void createConfigurationMenus(final JMenuBar menuBar) {
		final JMenu configurationMenu = new JMenu("Configuration");
		menuBar.add(configurationMenu);

		final JMenuItem addConfigurationItem = new JMenuItem("Add");
		addConfigurationItem.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent actionEvent) {
				Bibex.this.addConfiguration();
			}
		});
		addConfigurationItem.addMouseListener(this.mouseAdapter);
		addConfigurationItem.setToolTipText("Add a new configuration");
		configurationMenu.add(addConfigurationItem);

		final JMenuItem removeConfigurationItem = new JMenuItem("Remove");
		removeConfigurationItem.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent actionEvent) {
				Bibex.this.removeConfiguration();
			}
		});
		removeConfigurationItem.addMouseListener(this.mouseAdapter);
		removeConfigurationItem.setToolTipText("Remove this configuration");
		configurationMenu.add(removeConfigurationItem);

		configurationMenu.addSeparator();

		final JMenuItem loadConfigurationItem = new JMenuItem("Load");
		loadConfigurationItem.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent actionEvent) {
				Bibex.this.loadConfiguration();
			}
		});
		loadConfigurationItem.addMouseListener(this.mouseAdapter);
		loadConfigurationItem.setToolTipText("Load a configuration");
		configurationMenu.add(loadConfigurationItem);

		final JMenuItem saveConfigurationItem = new JMenuItem("Save");
		saveConfigurationItem.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent actionEvent) {
				Bibex.this.saveConfiguration();
			}
		});
		saveConfigurationItem.addMouseListener(this.mouseAdapter);
		saveConfigurationItem.setToolTipText("Save this configuration");
		configurationMenu.add(saveConfigurationItem);

		final JMenuItem saveConfigurationAsItem = new JMenuItem("Save as...");
		saveConfigurationAsItem.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent actionEvent) {
				Bibex.this.saveConfigurationAs();
			}
		});
		saveConfigurationAsItem.addMouseListener(this.mouseAdapter);
		saveConfigurationAsItem
			.setToolTipText("Save this configuration with a new name");
		configurationMenu.add(saveConfigurationAsItem);

		configurationMenu.addSeparator();

		final JMenuItem resetConfigurationItem = new JMenuItem("Reset");
		resetConfigurationItem.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent actionEvent) {
				Bibex.this.resetConfiguration();
			}
		});
		resetConfigurationItem.addMouseListener(this.mouseAdapter);
		resetConfigurationItem.setToolTipText("Reset this configuration");
		configurationMenu.add(resetConfigurationItem);

	}
	private void createHelpMenus(final JMenuBar menuBar) {
		final JMenu helpMenu = new JMenu("Help");
		menuBar.add(helpMenu);

		final JMenuItem aboutItem = new JMenuItem("About");
		aboutItem.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent actionEvent) {
				Bibex.this.about();
			}
		});
		aboutItem.addMouseListener(this.mouseAdapter);
		aboutItem.setToolTipText("About " + Bibex.BIBTEX_EXTRACTOR_NAME);
		helpMenu.add(aboutItem);

		final JMenuItem helpItem = new JMenuItem("Help");
		helpItem.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent actionEvent) {
				Bibex.this.help();
			}
		});
		helpItem.addMouseListener(this.mouseAdapter);
		helpItem.setToolTipText("Help on using " + Bibex.BIBTEX_EXTRACTOR_NAME);
		helpMenu.add(helpItem);
	}
	private void extract() {
		boolean isEverythingAllRight = true;
		if (this.currentExtractionDataSet.getRegexesSet().size() == 0) {
			isEverythingAllRight &= false;
			this
				.warnUser("At least one regex must be present to extract entries");
		}
		if (this.currentExtractionDataSet.getNewBibtexFile().equals("")) {
			isEverythingAllRight &= false;
			this.warnUser("A new BibTeX base must be given");
		}
		if (isEverythingAllRight) {
			try {
				final PrintWriter printWriter =
					new PrintWriter(new FileWriter(
						this.currentExtractionDataSet.getNewBibtexFile()));
				this.extract(printWriter);
			}
			catch (final IOException ioe) {
				this.warnUser(ioe.getMessage());
			}
		}
	}
	private void extract(final PrintWriter printWriter) {
		final Map fieldsMap =
			new HashMap(this.currentExtractionDataSet.getRegexesSet().size());
		final Set keysSet =
			new HashSet(this.currentExtractionDataSet.getRegexesSet().size());
		final Set typesSet =
			new HashSet(this.currentExtractionDataSet.getRegexesSet().size());

		final Iterator regexIterator =
			this.currentExtractionDataSet.getRegexesSet().iterator();
		while (regexIterator.hasNext()) {
			final Regex regex = (Regex) regexIterator.next();
			if (regex.getField().equals(Constants.KEY_NAME)) {
				keysSet.add(regex.getPattern());
			}
			else if (regex.getField().equals(Constants.TYPE_NAME)) {
				typesSet.add(regex.getPattern());
			}
			else {
				fieldsMap.put(regex.getField(), regex.getPattern());
			}
		}

		final Iterator iterator =
			this.currentExtractionDataSet.getBasesSet().iterator();
		while (iterator.hasNext()) {
			try {
				((Base) iterator.next()).getFile().accept(
					new BibtexExtractor(
						printWriter,
						typesSet,
						keysSet,
						fieldsMap,
						new ExtractorFunction() {
							public boolean isValid(
								final boolean entryTypeMatches,
								final boolean entryKeyMatches,
								final boolean keyValueMatches) {

								return entryTypeMatches && entryKeyMatches
										&& keyValueMatches;
							}
						}));
			}
			catch (final FileNotFoundException fnfe) {
				this.warnUser(fnfe.getMessage());
			}
			catch (final ParseException pe) {
				this.warnUser(pe.getMessage());
			}
			catch (final IOException ioe) {
				this.warnUser(ioe.getMessage());
			}
		}
		printWriter.close();
	}
	private void extractAll() {
		final ExtractionDataSet formerExtractionDataSet =
			this.currentExtractionDataSet;
		final Iterator iterator = this.extractionDataSets.iterator();
		while (iterator.hasNext()) {
			this.currentExtractionDataSet = (ExtractionDataSet) iterator.next();
			this.extract();
		}
		this.currentExtractionDataSet = formerExtractionDataSet;
	}
	private void help() {
		try {
			final StringBuffer docFullPath =
				new StringBuffer(System.getProperty("user.dir"));
			docFullPath.append(File.separatorChar);
			docFullPath.append("doc");
			docFullPath.append(File.separatorChar);
			docFullPath.append("Bibex.html");

			final LineNumberReader lineNumberReader =
				new LineNumberReader(new FileReader(docFullPath.toString()));
			final StringBuffer docBuffer = new StringBuffer();
			String line;
			while ((line = lineNumberReader.readLine()) != null) {
				docBuffer.append(line);
				docBuffer.append('\n');
			}
			lineNumberReader.close();

			final HelpWindow helpWindow =
				new HelpWindow("Help", docBuffer.toString());
			helpWindow.setVisible(true);
		}
		catch (final Exception e) {
			this.warnUser("Cannot find Bibex.doc file!");
		}
	}
	private String helpOnComponents(final JComponent component) {
		return component.getToolTipText();
	}
	private void informUser(final String message) {
		if (this.messageDuration == 0) {
			this.infoField.setForeground(Color.BLUE);
			this.infoField.setText(message);
		}
		else if (this.messageDuration > 0) {
			this.messageDuration--;
		}
	}
	private void loadConfiguration() {
		final JFileChooser fileChooser =
			new JFileChooser(this.currentExtractionDataSet.getCurrentPath());
		fileChooser.setFileFilter(Bibex.BIBEX_FILE_FILTER);
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooser.setMultiSelectionEnabled(false);
		if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {

			try {
				this.currentExtractionDataSet.setCurrentPath(fileChooser
					.getSelectedFile()
					.getPath());
				this.currentExtractionDataSet
					.setConfigurationFullPath(fileChooser
						.getSelectedFile()
						.getCanonicalPath());

				final FileInputStream fileInputStream =
					new FileInputStream(
						this.currentExtractionDataSet
							.getConfigurationFullPath());
				final ObjectInputStream objectInputStream =
					new ObjectInputStream(fileInputStream);

				this.currentExtractionDataSet.getBasesSet().clear();
				this.currentExtractionDataSet.getBasesSet().addAll(
					(Set) objectInputStream.readObject());

				this.currentExtractionDataSet.getRegexesSet().clear();
				this.currentExtractionDataSet.getRegexesSet().addAll(
					(Set) objectInputStream.readObject());

				this.currentExtractionDataSet
					.setNewBibtexFile((String) objectInputStream.readObject());

				objectInputStream.close();
				fileInputStream.close();
			}
			catch (final FileNotFoundException fnfe) {
				this.warnUser(fnfe.getMessage());
			}
			catch (final IOException ioe) {
				this.warnUser(ioe.getMessage());
			}
			catch (final ClassNotFoundException cnfe) {
				this.warnUser(cnfe.getMessage());
			}
		}

		this.updateDisplay();
	}
	private void loadObjectConfigurationBundle() {
		final JFileChooser fileChooser = new JFileChooser(this.bundleFullPath);
		fileChooser.setFileFilter(Bibex.BIBEX_BUNDLE_OBJECT_FILTER);
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooser.setMultiSelectionEnabled(false);
		if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {

			try {
				this.bundleFullPath =
					fileChooser.getSelectedFile().getCanonicalPath();

				final FileInputStream fileInputStream =
					new FileInputStream(this.bundleFullPath);
				final ObjectInputStream objectInputStream =
					new ObjectInputStream(fileInputStream);

				final int numberOfConfigurations = objectInputStream.readInt();

				// Yann 2004/07/18: Pretend!
				// I must pretend I am in the original state for bundles:
				// One (default) bundle, for extractionDataSets and
				// bundleModel.
				this.extractionDataSets.clear();
				this.extractionDataSets.add(new ExtractionDataSet());
				this.bundleModel.setValue(0);
				this.bundleModel.setMaximum(1);

				for (int i = 0; i < numberOfConfigurations; i++) {
					this.addConfiguration();

					this.currentExtractionDataSet
						.setConfigurationFullPath((String) objectInputStream
							.readObject());
					this.currentExtractionDataSet
						.setCurrentPath((String) objectInputStream.readObject());
					this.currentExtractionDataSet.getBasesSet().addAll(
						(Set) objectInputStream.readObject());
					this.currentExtractionDataSet.getRegexesSet().addAll(
						(Set) objectInputStream.readObject());
					this.currentExtractionDataSet
						.setNewBibtexFile((String) objectInputStream
							.readObject());
				}

				// The 0th extraction data set is a pretend...
				// I remove it!
				this.extractionDataSets.remove(0);
				this.bundleModel.setMaximum(numberOfConfigurations);
				this.bundleModel.setValue(this.bundleModel.getMaximum());

				objectInputStream.close();
				fileInputStream.close();
			}
			catch (final FileNotFoundException fnfe) {
				this.warnUser(fnfe.getMessage());
			}
			catch (final IOException ioe) {
				this.warnUser(ioe.getMessage());
			}
			catch (final ClassNotFoundException cnfe) {
				this.warnUser(cnfe.getMessage());
			}
		}

		this.updateDisplay();
	}
	private void loadXMLConfigurationBundle() {
		final JFileChooser fileChooser = new JFileChooser(this.bundleFullPath);
		fileChooser.setFileFilter(Bibex.BIBEX_BUNDLE_XML_FILTER);
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooser.setMultiSelectionEnabled(false);
		if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			try {
				this.bundleFullPath =
					fileChooser.getSelectedFile().getCanonicalPath();

				final LineNumberReader fileReader =
					new LineNumberReader(new FileReader(this.bundleFullPath));
				final StringBuffer buffer = new StringBuffer();
				String line;
				while ((line = fileReader.readLine()) != null) {
					buffer.append(line);
					buffer.append('\n');
				}
				fileReader.close();
				
				final String xmlString = buffer.toString();
				final XStream xstream = new XStream();
				final List dataSets = (List) xstream.fromXML(xmlString);

				this.extractionDataSets.clear();
				this.extractionDataSets.addAll(dataSets);
				this.currentExtractionDataSet =
					(ExtractionDataSet) this.extractionDataSets.get(0);

				this.bundleModel.setValue(0);
				this.bundleModel.setMaximum(this.extractionDataSets.size());
			}
			catch (final FileNotFoundException fnfe) {
				this.warnUser(fnfe.getMessage());
			}
			catch (final IOException ioe) {
				this.warnUser(ioe.getMessage());
			}
		}

		this.updateDisplay();
	}
	private void preview() {
		boolean isEverythingAllRight = true;
		if (this.currentExtractionDataSet.getRegexesSet().size() == 0) {
			isEverythingAllRight &= false;
			this
				.warnUser("At least one regex must be present to extract entries");
		}
		if (this.currentExtractionDataSet.getNewBibtexFile().equals("")) {
			this.warnUser("A new BibTeX base must be given");
		}
		if (isEverythingAllRight) {
			final StringWriter stringWriter = new StringWriter();
			this.extract(new PrintWriter(stringWriter));
			final StringBuffer title = new StringBuffer("Preview");
			title.append(" - ");
			title.append(this.currentExtractionDataSet.getName());
			title.append(" (configuration number ");
			title.append(this.extractionDataSets
				.indexOf(this.currentExtractionDataSet));
			title.append(')');
			final InfoWindow previewWindow =
				new InfoWindow(title.toString(), stringWriter.toString());
			previewWindow.setVisible(true);
		}
	}
	private void previewAll() {
		final ExtractionDataSet formerExtractionDataSet =
			this.currentExtractionDataSet;
		final Iterator iterator = this.extractionDataSets.iterator();
		while (iterator.hasNext()) {
			this.currentExtractionDataSet = (ExtractionDataSet) iterator.next();
			this.preview();
		}
		this.currentExtractionDataSet = formerExtractionDataSet;
	}
	private void removeBase() {
		if (this.basesList.isSelectionEmpty()) {
			this.warnUser("Select at least one base to remove");
		}
		else {
			final Object[] selectedValues =
				this.basesList.getSelectedValuesList().toArray();
			for (int i = 0; i < selectedValues.length; i++) {
				this.currentExtractionDataSet.getBasesSet().remove(
					selectedValues[i]);
			}
			final Object[] bases =
				this.currentExtractionDataSet.getBasesSet().toArray();
			Arrays.sort(bases);
			this.basesList.setListData(bases);
			this.cacheBaseData();
			if (selectedValues.length > 1) {
				this.informUser("Bases removed");
			}
			else {
				this.informUser("Base removed");
			}
		}
	}
	private void removeConfiguration() {
		if (this.bundleModel.getMaximum() > 1) {
			this.extractionDataSets.remove(this.bundleModel.getValue());
			this.bundleModel.setMaximum(this.bundleModel.getMaximum() - 1);

			this.currentExtractionDataSet =
				(ExtractionDataSet) this.extractionDataSets
					.get(this.bundleModel.getValue());
		}
		else {
			this.extractionDataSets.remove(0);
			this.currentExtractionDataSet = new ExtractionDataSet();
			this.extractionDataSets.add(this.currentExtractionDataSet);
		}

		this.updateDisplay();
	}
	private void removeRegex() {
		if (this.regexesList.isSelectionEmpty()) {
			this.warnUser("Select at least one regex to remove");
		}
		else {
			final Object[] selectedValues =
				this.regexesList.getSelectedValuesList().toArray();
			for (int i = 0; i < selectedValues.length; i++) {
				this.currentExtractionDataSet.getRegexesSet().remove(
					selectedValues[i]);
			}
			final Object[] regexes =
				this.currentExtractionDataSet.getRegexesSet().toArray();
			Arrays.sort(regexes);
			this.regexesList.setListData(regexes);
			if (selectedValues.length > 1) {
				this.informUser("Regexes removed");
			}
			else {
				this.informUser("Regex removed");
			}
		}
	}
	private void resetConfiguration() {
		this.currentExtractionDataSet.setConfigurationFullPath("");
		this.setTitle();
	}
	private void resetConfigurationBundle() {
		this.bundleFullPath = "";
		this.setTitle();
	}
	private void saveConfiguration() {
		if (this.currentExtractionDataSet.getConfigurationFullPath().equals("")) {

			this.saveConfigurationAs();
		}
		else {
			this.saveConfiguration(this.currentExtractionDataSet
				.getConfigurationFullPath());
		}
	}
	private void saveConfiguration(final String fullPath) {
		try {
			final FileOutputStream fileOutputStream =
				new FileOutputStream(fullPath);
			final ObjectOutputStream objectOutputStream =
				new ObjectOutputStream(fileOutputStream);

			objectOutputStream.writeObject(this.currentExtractionDataSet
				.getBasesSet());
			objectOutputStream.writeObject(this.currentExtractionDataSet
				.getRegexesSet());
			objectOutputStream.writeObject(this.currentExtractionDataSet
				.getNewBibtexFile());

			objectOutputStream.flush();
			fileOutputStream.close();
		}
		catch (final FileNotFoundException fnfe) {
			this.warnUser(fnfe.getMessage());
		}
		catch (final IOException ioe) {
			this.warnUser(ioe.getMessage());
		}

		this.setTitle();
	}
	private void saveConfigurationAs() {
		final JFileChooser fileChooser =
			new JFileChooser(this.currentExtractionDataSet.getCurrentPath());
		fileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
		fileChooser.setFileFilter(Bibex.BIBEX_FILE_FILTER);
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooser.setMultiSelectionEnabled(false);
		if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {

			try {
				this.currentExtractionDataSet.setCurrentPath(fileChooser
					.getSelectedFile()
					.getPath());

				this.currentExtractionDataSet
					.setConfigurationFullPath(fileChooser
						.getSelectedFile()
						.getCanonicalPath());
				this.saveConfiguration(this.currentExtractionDataSet
					.getConfigurationFullPath());
			}
			catch (final IOException ioe) {
				this.warnUser(ioe.getMessage());
			}
		}
	}
	private void saveObjectConfigurationBundle() {
		if (this.bundleFullPath.equals("")) {
			this.saveObjectConfigurationBundleAs();
		}
		else {
			this.saveObjectConfigurationBundle(this.bundleFullPath);
		}
	}
	private void saveObjectConfigurationBundle(final String fullPath) {
		try {
			final FileOutputStream fileOutputStream =
				new FileOutputStream(fullPath);
			final ObjectOutputStream objectOutputStream =
				new ObjectOutputStream(fileOutputStream);

			objectOutputStream.writeInt(this.extractionDataSets.size());

			final Iterator iterator = this.extractionDataSets.iterator();
			while (iterator.hasNext()) {
				final ExtractionDataSet extractionDataSet =
					(ExtractionDataSet) iterator.next();

				objectOutputStream.writeObject(extractionDataSet
					.getConfigurationFullPath());
				objectOutputStream.writeObject(extractionDataSet
					.getCurrentPath());
				objectOutputStream.writeObject(extractionDataSet.getBasesSet());
				objectOutputStream.writeObject(extractionDataSet
					.getRegexesSet());
				objectOutputStream.writeObject(extractionDataSet
					.getNewBibtexFile());
			}

			objectOutputStream.flush();
			fileOutputStream.close();
		}
		catch (final FileNotFoundException fnfe) {
			this.warnUser(fnfe.getMessage());
		}
		catch (final IOException ioe) {
			this.warnUser(ioe.getMessage());
		}

		this.setTitle();
	}
	private void saveObjectConfigurationBundleAs() {
		final JFileChooser fileChooser = new JFileChooser(this.bundleFullPath);
		fileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
		fileChooser.setFileFilter(Bibex.BIBEX_BUNDLE_OBJECT_FILTER);
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooser.setMultiSelectionEnabled(false);
		if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {

			try {
				this.bundleFullPath =
					fileChooser.getSelectedFile().getCanonicalPath();
				this.saveObjectConfigurationBundle(this.bundleFullPath);
			}
			catch (final IOException ioe) {
				this.warnUser(ioe.getMessage());
			}
		}
	}
	private void saveXMLConfigurationBundle() {
		if (this.bundleFullPath.equals("")) {
			this.saveXMLConfigurationBundleAs();
		}
		else {
			this.saveXMLConfigurationBundle(this.bundleFullPath);
		}
	}
	private void saveXMLConfigurationBundle(final String fullPath) {
		final XStream xstream = new XStream();
		xstream.omitField(Base.class, "file");
		final String xmlString = xstream.toXML(this.extractionDataSets);

		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(fullPath);
			fileWriter.write(xmlString);
			fileWriter.close();
		}
		catch (final IOException ioe) {
			this.warnUser(ioe.getMessage());
		}
		finally {
			if (fileWriter != null) {
				try {
					fileWriter.close();
				}
				catch (IOException ioe) {
					this.warnUser(ioe.getMessage());
				}
			}
		}
	}
	private void saveXMLConfigurationBundleAs() {
		final JFileChooser fileChooser = new JFileChooser(this.bundleFullPath);
		fileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
		fileChooser.setFileFilter(Bibex.BIBEX_BUNDLE_XML_FILTER);
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooser.setMultiSelectionEnabled(false);
		if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			try {
				this.bundleFullPath =
					fileChooser.getSelectedFile().getCanonicalPath();
				this.saveXMLConfigurationBundle(this.bundleFullPath);
			}
			catch (final IOException ioe) {
				this.warnUser(ioe.getMessage());
			}
		}
	}
	private void setTitle() {
		final StringBuffer buffer = new StringBuffer(50);
		buffer.append(Bibex.BIBTEX_EXTRACTOR_NAME);
		if (!this.bundleFullPath.equals("")) {

			buffer.append(" - [");
			buffer.append(this.bundleFullPath.substring(
				this.bundleFullPath.lastIndexOf(File.separatorChar) + 1,
				this.bundleFullPath.length()));
			buffer.append(']');
		}
		if (!this.currentExtractionDataSet
			.getConfigurationFullPath()
			.equals("")) {

			buffer.append(" - ");
			buffer.append(this.currentExtractionDataSet.getName());
		}

		super.setTitle(buffer.toString());
	}
	public void setTitle(final String configurationFullPath) {
		this.setTitle();
	}
	private void start() {
		final Preferences prefs =
			Preferences.userRoot().node(this.getClass().getName());
		this.bundleFullPath = prefs.get(Bibex.PREFS_KEY_BUNDLE_FULL_PATH, "");
		this.windowXPosition =
			prefs
				.getInt(
					Bibex.PREFS_KEY_WINDOW_X_POSITION,
					(int) (Constants.SCREEN_DIMENSION.getWidth() / 2 - Bibex.WINDOW_DIMENSION
						.getWidth() / 2));
		this.windowYPosition =
			prefs
				.getInt(
					Bibex.PREFS_KEY_WINDOW_Y_POSITION,
					(int) (Constants.SCREEN_DIMENSION.getHeight() / 2 - Bibex.WINDOW_DIMENSION
						.getHeight() / 2));
	}
	private void stop() {
		final Preferences prefs =
			Preferences.userRoot().node(this.getClass().getName());
		prefs.put(Bibex.PREFS_KEY_BUNDLE_FULL_PATH, this.bundleFullPath);
		prefs.putInt(Bibex.PREFS_KEY_WINDOW_X_POSITION, (int) this
			.getLocation()
			.getX());
		prefs.putInt(Bibex.PREFS_KEY_WINDOW_Y_POSITION, (int) this
			.getLocation()
			.getY());
		System.exit(0);
	}
	private void updateDisplay() {
		this.setTitle();

		this.basesList.removeAll();
		this.basesList.setListData(this.currentExtractionDataSet
			.getBasesSet()
			.toArray());

		this.regexesList.removeAll();
		this.regexesList.setListData(this.currentExtractionDataSet
			.getRegexesSet()
			.toArray());

		this.newBibtexFileField.setText(this.currentExtractionDataSet
			.getNewBibtexFile());

		this.cacheBaseData();
	}
	private void warnUser(final String message) {
		this.infoField.setForeground(Color.RED);
		this.infoField.setText(message);
		this.messageDuration = Bibex.WARNING_DURATION;
	}
}
