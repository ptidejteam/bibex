/*
 * (c) Copyright 2001-2003 Yann-Ga�l Gu�h�neuc,
 * �cole des Mines de Nantes and Object Technology International, Inc.
 * 
 * Use and copying of this software and preparation of derivative works
 * based upon this software are permitted. Any copy of this software or
 * of any derivative work must include the above copyright notice of
 * the author, this paragraph and the one after it.
 * 
 * This software is made available AS IS, and THE AUTHOR DISCLAIMS
 * ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, AND NOT WITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN,
 * ANY LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF THE AUTHOR IS ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 * 
 * All Rights Reserved.
 */
package bibex.extractor.ui;

import java.io.Serializable;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * @author 	Yann-Ga�l Gu�h�neuc
 */
class Regex implements Comparable, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8778209543882784416L;
	private final String field;
	private final String value;
	private final Pattern pattern;

	public Regex(final String field, final String value)
			throws IllegalArgumentException, PatternSyntaxException {

		this.field = field;
		this.value = value;
		this.pattern = Pattern.compile(this.value, Pattern.CASE_INSENSITIVE);
	}
	public int compareTo(final Object o) {
		return this.toString().compareTo(o.toString());
	}
	public boolean equals(final Object o) {
		return this.toString().equals(o.toString());
	}
	public String getField() {
		return this.field;
	}
	public Pattern getPattern() {
		return this.pattern;
	}
	public String getValue() {
		return this.value;
	}
	public int hashCode() {
		return this.toString().hashCode();
	}
	public String toString() {
		return this.getField() + " = " + this.getValue();
	}
}
