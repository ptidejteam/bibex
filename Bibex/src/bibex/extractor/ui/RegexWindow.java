/*
 * (c) Copyright 2001-2003 Yann-Ga�l Gu�h�neuc,
 * �cole des Mines de Nantes and Object Technology International, Inc.
 * 
 * Use and copying of this software and preparation of derivative works
 * based upon this software are permitted. Any copy of this software or
 * of any derivative work must include the above copyright notice of
 * the author, this paragraph and the one after it.
 * 
 * This software is made available AS IS, and THE AUTHOR DISCLAIMS
 * ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, AND NOT WITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN,
 * ANY LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF THE AUTHOR IS ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 * 
 * All Rights Reserved.
 */
package bibex.extractor.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.PatternSyntaxException;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * @author 	Yann-Ga�l Gu�h�neuc
 */
class RegexWindow extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1092959673638102542L;
	private static RegexWindow UNIQUE_INSTANCE;
	private static final int WARNING_DURATION = 2;
	private static final Dimension WINDOW_DIMENSION = new Dimension(600, 124);

	public static RegexWindow getUniqueInstance() {
		if (RegexWindow.UNIQUE_INSTANCE == null) {
			RegexWindow.UNIQUE_INSTANCE = new RegexWindow();
		}
		return RegexWindow.UNIQUE_INSTANCE;
	}
	private final JComboBox fieldComboxBox;
	private final JTextField infoField;
	private final JComboBox keyComboxBox;
	private int messageDuration;
	private final Set regexWindowListeners = new HashSet(1);

	private final JComboBox typeComboxBox;

	private final MouseAdapter mouseAdapter = new MouseAdapter() {
		public void mouseEntered(final MouseEvent mouseEvent) {
			RegexWindow.this.informUser(RegexWindow.this
				.helpOnComponents((JComponent) mouseEvent.getComponent()));
		}
		public void mouseExited(final MouseEvent mouseEvent) {
			RegexWindow.this.informUser("");
		}
	};

	private RegexWindow() {
		this.getContentPane().setLayout(
			new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		this
			.setLocation(
				(int) (Constants.SCREEN_DIMENSION.getWidth() / 2 - RegexWindow.WINDOW_DIMENSION
					.getWidth() / 2),
				(int) (Constants.SCREEN_DIMENSION.getHeight() / 2 - RegexWindow.WINDOW_DIMENSION
					.getHeight() / 2) + 212);
		this.setResizable(false);
		this.setSize(RegexWindow.WINDOW_DIMENSION);
		this.setTitle("Regexes");

		/* +--------------------+ */
		/* | Widgets for fields | */
		/* +--------------------+ */

		final JPanel fieldPanel = new JPanel();
		fieldPanel.setLayout(new BoxLayout(fieldPanel, BoxLayout.X_AXIS));
		this.getContentPane().add(fieldPanel);

		this.fieldComboxBox = new JComboBox();
		this.fieldComboxBox.addMouseListener(this.mouseAdapter);
		this.fieldComboxBox
			.setToolTipText("List of existing fields in the BibTeX bases");
		fieldPanel.add(this.fieldComboxBox);

		final JLabel fieldEqualLabel = new JLabel(" = ");
		fieldPanel.add(fieldEqualLabel);

		final JTextField fieldValueTextField = new JTextField();
		fieldValueTextField.addMouseListener(this.mouseAdapter);
		fieldValueTextField.setToolTipText("A regex on the selected field");
		fieldPanel.add(fieldValueTextField);

		final JButton fieldAddButton = new JButton("Add");
		fieldAddButton.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent actionEvent) {
				if (!fieldValueTextField.getText().equals("")) {
					try {
						final Regex regex =
							new Regex((String) RegexWindow.this.fieldComboxBox
								.getSelectedItem(), fieldValueTextField
								.getText());

						final Iterator iterator =
							RegexWindow.this.regexWindowListeners.iterator();
						while (iterator.hasNext()) {
							((RegexWindowListener) iterator.next())
								.regexAdded(regex);
						}
						RegexWindow.this.informUser("Regex submitted");
					}
					catch (final PatternSyntaxException pse) {
						RegexWindow.this.warnUser(pse.getMessage());
					}
				}
				else {
					RegexWindow.this.warnUser("Cannot add empty regex");
				}
			}
		});
		fieldAddButton.addMouseListener(this.mouseAdapter);
		fieldAddButton.setToolTipText("Add this regex to the list");
		fieldPanel.add(fieldAddButton);

		/* +------------------+ */
		/* | Widgets for keys | */
		/* +------------------+ */

		final JPanel keyPanel = new JPanel();
		keyPanel.setLayout(new BoxLayout(keyPanel, BoxLayout.X_AXIS));
		this.getContentPane().add(keyPanel);

		this.keyComboxBox = new JComboBox();
		this.keyComboxBox.addMouseListener(this.mouseAdapter);
		this.keyComboxBox.setEditable(true);
		this.keyComboxBox
			.setToolTipText("List of existing keys in the BibTeX bases");
		keyPanel.add(this.keyComboxBox);

		final JButton keyAddButton = new JButton("Add");
		keyAddButton.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent actionEvent) {
				try {
					final String selectedItem =
						(String) RegexWindow.this.keyComboxBox
							.getSelectedItem();
					if (selectedItem != null) {
						final Regex regex =
							new Regex(Constants.KEY_NAME, selectedItem);

						final Iterator iterator =
							RegexWindow.this.regexWindowListeners.iterator();
						while (iterator.hasNext()) {
							((RegexWindowListener) iterator.next())
								.regexAdded(regex);
						}
						RegexWindow.this.informUser("Regex submitted");
					}
					else {
						RegexWindow.this.warnUser("Cannot add empty regex");
					}
				}
				catch (final PatternSyntaxException pse) {
					RegexWindow.this.warnUser(pse.getMessage());
				}
			}
		});
		keyAddButton.addMouseListener(this.mouseAdapter);
		keyAddButton.setToolTipText("Add this regex to the list");
		keyPanel.add(keyAddButton);

		/* +-------------------+ */
		/* | Widgets for types | */
		/* +-------------------+ */

		final JPanel typePanel = new JPanel();
		typePanel.setLayout(new BoxLayout(typePanel, BoxLayout.X_AXIS));
		this.getContentPane().add(typePanel);

		this.typeComboxBox = new JComboBox();
		this.typeComboxBox.addMouseListener(this.mouseAdapter);
		this.typeComboxBox.setEditable(true);
		this.typeComboxBox
			.setToolTipText("List of existing types in the BibTeX bases");
		typePanel.add(this.typeComboxBox);

		final JButton typeAddButton = new JButton("Add");
		typeAddButton.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent actionEvent) {
				try {
					final String selectedItem =
						(String) RegexWindow.this.typeComboxBox
							.getSelectedItem();
					if (selectedItem != null) {
						final Regex regex =
							new Regex(Constants.TYPE_NAME, selectedItem);

						final Iterator iterator =
							RegexWindow.this.regexWindowListeners.iterator();
						while (iterator.hasNext()) {
							((RegexWindowListener) iterator.next())
								.regexAdded(regex);
						}
						RegexWindow.this.informUser("Regex submitted");
					}
					else {
						RegexWindow.this.warnUser("Cannot add empty regex");
					}
				}
				catch (final PatternSyntaxException pse) {
					RegexWindow.this.warnUser(pse.getMessage());
				}
			}
		});
		typeAddButton.addMouseListener(this.mouseAdapter);
		typeAddButton.setToolTipText("Add this regex to the list");
		typePanel.add(typeAddButton);

		// Yann 2003/07/05: Ugly!
		// Is there anyway to remove the declaration of
		// the infoField *outside* of this constructor,
		// not to pollute the class declaration?
		this.infoField = new JTextField();
		this.infoField.setEditable(false);
		this.getContentPane().add(this.infoField);
	}
	public void addRegexWindowListener(
		final RegexWindowListener regexWindowListener) {
		this.regexWindowListeners.add(regexWindowListener);
	}
	private String helpOnComponents(final JComponent component) {
		return component.getToolTipText();
	}
	private void informUser(final String message) {
		if (this.messageDuration == 0) {
			this.infoField.setForeground(Color.BLUE);
			this.infoField.setText(message);
		}
		else if (this.messageDuration > 0) {
			this.messageDuration--;
		}
	}
	public void removeRegexWindowListener(
		final RegexWindowListener regexWindowListener) {
		this.regexWindowListeners.remove(regexWindowListener);
	}
	public void setFields(final Set fieldsSet) {
		this.fieldComboxBox.removeAllItems();
		final Object[] fields = fieldsSet.toArray();
		Arrays.sort(fields);
		for (int i = 0; i < fields.length; i++) {
			this.fieldComboxBox.addItem(fields[i]);
		}
		if (fields.length > 0) {
			this.fieldComboxBox.setSelectedIndex(0);
		}
		this.fieldComboxBox.repaint();
	}
	public void setKeys(final Set keysSet) {
		this.keyComboxBox.removeAllItems();
		final Object[] keys = keysSet.toArray();
		Arrays.sort(keys);
		for (int i = 0; i < keys.length; i++) {
			this.keyComboxBox.addItem(keys[i]);
		}
		if (keys.length > 0) {
			this.keyComboxBox.setSelectedIndex(0);
		}
		this.keyComboxBox.repaint();
	}
	public void setTypes(final Set typesSet) {
		this.typeComboxBox.removeAllItems();
		final Object[] types = typesSet.toArray();
		Arrays.sort(types);
		for (int i = 0; i < types.length; i++) {
			this.typeComboxBox.addItem(types[i]);
		}
		if (types.length > 0) {
			this.typeComboxBox.setSelectedIndex(0);
		}
		this.typeComboxBox.repaint();
	}
	private void warnUser(final String message) {
		this.infoField.setForeground(Color.RED);
		this.infoField.setText(message);
		this.messageDuration = RegexWindow.WARNING_DURATION;
	}
}
