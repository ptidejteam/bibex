/*
 * (c) Copyright 2001-2003 Yann-Ga�l Gu�h�neuc,
 * �cole des Mines de Nantes and Object Technology International, Inc.
 * 
 * Use and copying of this software and preparation of derivative works
 * based upon this software are permitted. Any copy of this software or
 * of any derivative work must include the above copyright notice of
 * the author, this paragraph and the one after it.
 * 
 * This software is made available AS IS, and THE AUTHOR DISCLAIMS
 * ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, AND NOT WITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN,
 * ANY LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF THE AUTHOR IS ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 * 
 * All Rights Reserved.
 */
package bibex.extractor.ui;

import java.util.HashSet;
import java.util.Set;
import bibex.dom.BibtexConcatenatedValue;
import bibex.dom.BibtexEntry;
import bibex.dom.BibtexField;
import bibex.dom.BibtexFile;
import bibex.dom.BibtexKey;
import bibex.dom.BibtexPreamble;
import bibex.dom.BibtexString;
import bibex.dom.BibtexStringDefinition;
import bibex.dom.BibtexStringReference;
import bibex.dom.BibtexToplevelComment;
import bibex.visitor.BibtexVisitor;

/**
 * @author 	Yann-Ga�l Gu�h�neuc
 */

// This public class only exists because of a bug in
// JDK 1.4.1_03! See the UI.java file (method addRegex()).
public class BaseDataCollector implements BibtexVisitor {
	private final Set fieldsSet = new HashSet(10);
	private final Set keysSet = new HashSet(10);
	private final Set typesSet = new HashSet(10);

	public void close(final BibtexEntry bibtexEntry) {
	}
	public void close(final BibtexField bibtexField) {
	}
	public void close(final BibtexFile bibtexFile) {
	}
	public Set getFields() {
		return this.fieldsSet;
	}
	public Set getKeys() {
		return this.keysSet;
	}
	public Set getTypes() {
		return this.typesSet;
	}
	public void open(final BibtexEntry bibtexEntry) {
		this.keysSet.add(bibtexEntry.getKey());
		this.typesSet.add(bibtexEntry.getType());
	}
	public void open(final BibtexField bibtexField) {
	}
	public void open(final BibtexFile bibtexFile) {
		this.fieldsSet.clear();
		this.keysSet.clear();
		this.typesSet.clear();
	}
	public void visit(final BibtexConcatenatedValue bibtexConcatenatedValue) {
	}
	public void visit(final BibtexKey bibtexKey) {
		this.fieldsSet.add(bibtexKey.getValue());
	}
	public void visit(final BibtexPreamble bibtexPreamble) {
	}
	public void visit(final BibtexString bibtexString) {
	}
	public void visit(final BibtexStringDefinition bibtexStringDefinition) {
	}
	public void visit(final BibtexStringReference bibtexStringReference) {
	}
	public void visit(final BibtexToplevelComment bibtexToplevelComment) {
	}
}