/*
 * (c) Copyright 2001-2003 Yann-Ga�l Gu�h�neuc,
 * �cole des Mines de Nantes and Object Technology International, Inc.
 * 
 * Use and copying of this software and preparation of derivative works
 * based upon this software are permitted. Any copy of this software or
 * of any derivative work must include the above copyright notice of
 * the author, this paragraph and the one after it.
 * 
 * This software is made available AS IS, and THE AUTHOR DISCLAIMS
 * ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, AND NOT WITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN,
 * ANY LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF THE AUTHOR IS ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 * 
 * All Rights Reserved.
 */
package bibex.extractor.ui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import bibex.parser.ParseException;

/**
 * @author Yann-Ga�l Gu�h�neuc
 * @since  2004/07/17
 */
public class ExtractionDataSet {
	// Cache for bases data.
	private final Set basesSet = new HashSet(3);
	// Path to the configuration file, if any.
	private String configurationFullPath = "";
	// Current path for FileDialog.
	private String currentPath = System.getProperty("user.dir");
	// Cache for bases data.
	private final Set fieldsSet = new HashSet(10);
	// Cache for bases data.
	private final Set keysSet = new HashSet(10);
	// Path to the new BibTeX base, if any.
	private String newBibtexFile = "";
	// Set of instances of Regex.
	private final Set regexesSet = new HashSet(3);
	// Cache for bases data.
	private final Set typesSet = new HashSet(10);

	public void cacheBaseData() {
		final BaseDataCollector baseInfoCollector = new BaseDataCollector();
		final Iterator iterator = this.getBasesSet().iterator();
		while (iterator.hasNext()) {
			// Yann 2003/07/05: Bug in JDK 1.4.1_03!?
			// For some reasons, I cannot put directly here
			// an anonymous class implementing interface BibtexVisitor.
			// The call using reflection in BibtexNode throws:
			// 		java.lang.IllegalAccessException: Class bibtex.dom.BibtexNode
			// 		can not access a member of class bibtex.extractor.example.UI$3
			// 		with modifiers "public"
			//			at sun.reflect.Reflection.ensureMemberAccess(Reflection.java:57)
			//			at java.lang.reflect.Method.invoke(Method.java:317)
			//			at bibtex.dom.BibtexNode.accept(BibtexNode.java:34)
			//			at bibtex.dom.BibtexNode.accept(BibtexNode.java:23)
			//			at bibtex.dom.BibtexField.accept(BibtexField.java:45)
			//			at bibtex.dom.BibtexEntry.accept(BibtexEntry.java:43)
			//			at bibtex.dom.BibtexFile.accept(BibtexFile.java:93)
			//			at bibtex.extractor.example.UI.addRegex(UI.java:202)
			//			...
			// I cannot put even the FieldCollector class in this
			// file: It must be "public"!?
			try {
				((Base) iterator.next()).getFile().accept(baseInfoCollector);
				final Object[] fields = baseInfoCollector.getFields().toArray();
				Arrays.sort(fields);
				for (int j = 0; j < fields.length; j++) {
					this.fieldsSet.add(fields[j]);
				}
				final Object[] keys = baseInfoCollector.getKeys().toArray();
				Arrays.sort(keys);
				for (int j = 0; j < keys.length; j++) {
					this.keysSet.add(keys[j]);
				}
				final Object[] types = baseInfoCollector.getTypes().toArray();
				Arrays.sort(types);
				for (int j = 0; j < types.length; j++) {
					this.typesSet.add(types[j]);
				}
			}
			catch (final FileNotFoundException fnfe) {
				fnfe.printStackTrace();
			}
			catch (final ParseException pe) {
				pe.printStackTrace();
			}
			catch (final IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}
	public Set getBasesSet() {
		return this.basesSet;
	}
	public String getConfigurationFullPath() {
		return this.configurationFullPath;
	}
	public String getCurrentPath() {
		return this.currentPath;
	}
	public Set getFieldsSet() {
		return this.fieldsSet;
	}
	public Set getKeysSet() {
		return this.keysSet;
	}
	public String getName() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append('[');
		buffer
			.append(this
				.getConfigurationFullPath()
				.substring(
					this.getConfigurationFullPath().lastIndexOf(
						File.separatorChar) + 1));
		buffer.append(']');
		return buffer.toString();
	}
	public String getNewBibtexFile() {
		return this.newBibtexFile;
	}
	public Set getRegexesSet() {
		return this.regexesSet;
	}
	public Set getTypesSet() {
		return this.typesSet;
	}
	public void setConfigurationFullPath(final String configurationFullPath) {
		this.configurationFullPath = configurationFullPath;
	}
	public void setCurrentPath(final String currentPath) {
		this.currentPath = currentPath;
	}
	public void setNewBibtexFile(final String newBibtexFile) {
		this.newBibtexFile = newBibtexFile;
	}
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append(this.getName());
		buffer.append("\n\t");
		buffer.append(this.getConfigurationFullPath());
		buffer.append("\n\t");
		buffer.append(this.getNewBibtexFile());
		buffer.append("\n\t");
		buffer.append(this.getRegexesSet());
		buffer.append("\n\t");
		buffer.append(this.getFieldsSet());
		buffer.append("\n\t");
		buffer.append(this.getKeysSet());
		buffer.append("\n\t");
		buffer.append(this.getTypesSet());
		buffer.append('\n');
		return buffer.toString();
	}
}
