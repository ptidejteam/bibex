/*
 * (c) Copyright 2001-2003 Yann-Ga�l Gu�h�neuc,
 * �cole des Mines de Nantes and Object Technology International, Inc.
 * 
 * Use and copying of this software and preparation of derivative works
 * based upon this software are permitted. Any copy of this software or
 * of any derivative work must include the above copyright notice of
 * the author, this paragraph and the one after it.
 * 
 * This software is made available AS IS, and THE AUTHOR DISCLAIMS
 * ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, AND NOT WITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN,
 * ANY LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF THE AUTHOR IS ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 * 
 * All Rights Reserved.
 */
package bibex.extractor.ui;

import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * @author 	Yann-Ga�l Gu�h�neuc
 */
class InfoWindow extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3177775237245698603L;
	private static final Dimension WINDOW_DIMENSION = new Dimension(600, 400);

	public InfoWindow(final String title, final String text) {
		this
			.setLocation(
				(int) (Constants.SCREEN_DIMENSION.getWidth() / 2 - InfoWindow.WINDOW_DIMENSION
					.getWidth() / 2),
				(int) (Constants.SCREEN_DIMENSION.getHeight() / 2 - InfoWindow.WINDOW_DIMENSION
					.getHeight() / 2));
		this.setSize(InfoWindow.WINDOW_DIMENSION);
		this.setTitle(title);

		final JTextArea infoTextArea = new JTextArea();
		infoTextArea.setEditable(false);
		infoTextArea.setText(text);
		this.getContentPane().add(new JScrollPane(infoTextArea));
	}
}
