/*
 * (c) Copyright 2001-2003 Yann-Ga�l Gu�h�neuc,
 * �cole des Mines de Nantes and Object Technology International, Inc.
 * 
 * Use and copying of this software and preparation of derivative works
 * based upon this software are permitted. Any copy of this software or
 * of any derivative work must include the above copyright notice of
 * the author, this paragraph and the one after it.
 * 
 * This software is made available AS IS, and THE AUTHOR DISCLAIMS
 * ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, AND NOT WITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN,
 * ANY LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF THE AUTHOR IS ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 * 
 * All Rights Reserved.
 */
package bibex.extractor.ui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import bibex.dom.BibtexFile;
import bibex.parser.ParseException;

/**
 * @author 	Yann-Ga�l Gu�h�neuc
 */
class Base implements Comparable, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3535659411338549654L;
	private long cachedTimestamp;
	private volatile BibtexFile file;
	private final String name;
	private final String path;

	public Base(final String name, final String fullPath)
			throws FileNotFoundException, ParseException, IOException {

		this.name = name;
		this.path = fullPath;
		this.cachedTimestamp = this.getActualTimestamp();
	}
	public int compareTo(final Object o) {
		return this.toString().compareTo(o.toString());
	}
	public boolean equals(final Object o) {
		return this.toString().equals(o.toString());
	}
	private long getActualTimestamp() {
		return new File(this.getFullPath()).lastModified();
	}
	public BibtexFile getFile() throws FileNotFoundException, ParseException,
			IOException {

		if (this.file == null
				|| this.cachedTimestamp != this.getActualTimestamp()) {

			this.refresh();
		}
		return this.file;
	}
	public String getFullPath() {
		return this.path;
	}
	public String getName() {
		return this.name;
	}
	public int hashCode() {
		return this.toString().hashCode();
	}
	private void refresh() throws FileNotFoundException, ParseException,
			IOException {

		this.cachedTimestamp = this.getActualTimestamp();
		if (this.file == null) {
			this.file = new BibtexFile();
		}
		else {
			this.file.reset();
		}
		Constants.BIBTEX_PARSER.parse(this.getFile(), new FileReader(this
			.getFullPath()), true);
	}
	public String toString() {
		return this.getName();
	}
}
