/*
 * (c) Copyright 2001-2003 Yann-Ga�l Gu�h�neuc,
 * �cole des Mines de Nantes and Object Technology International, Inc.
 * 
 * Use and copying of this software and preparation of derivative works
 * based upon this software are permitted. Any copy of this software or
 * of any derivative work must include the above copyright notice of
 * the author, this paragraph and the one after it.
 * 
 * This software is made available AS IS, and THE AUTHOR DISCLAIMS
 * ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, AND NOT WITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN,
 * ANY LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF THE AUTHOR IS ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 * 
 * All Rights Reserved.
 */
package bibex.extractor.simple;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.regex.Pattern;
import bibex.dom.BibtexFile;
import bibex.extractor.BibtexExtractor;
import bibex.extractor.ExtractorFunction;
import bibex.parser.BibtexParser;
import bibex.parser.ParseException;

/**
 * @author 	Yann-Ga�l Gu�h�neuc
 */
public class Main {
	public static void main(final String[] args) {
		try {
			final BibtexFile bibtexFile = new BibtexFile();
			final BibtexParser bibtexParser = new BibtexParser();
			final PrintWriter writer = new PrintWriter(System.out);

			bibtexParser.parse(bibtexFile, new FileReader(System
				.getProperty("user.dir")
					+ File.separatorChar + "Base/Books.bib"), true);

			final Map keyValueMap = new HashMap(1);
			keyValueMap.put("kind", Pattern.compile(
				"DIVERS",
				Pattern.CASE_INSENSITIVE));
			bibtexFile.accept(new BibtexExtractor(
				writer,
				new HashSet(0),
				new HashSet(0),
				keyValueMap,
				new ExtractorFunction() {
					public boolean isValid(
						final boolean entryTypeMatches,
						final boolean entryKeyMatches,
						final boolean keyValueMatches) {

						return entryTypeMatches && entryKeyMatches
								&& keyValueMatches;
					}
				}));
		}
		catch (final FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (final ParseException e) {
			e.printStackTrace();
		}
		catch (final IOException e) {
			e.printStackTrace();
		}
	}
}
