/*
 * (c) Copyright 2001-2003 Yann-Ga�l Gu�h�neuc,
 * �cole des Mines de Nantes and Object Technology International, Inc.
 * 
 * Use and copying of this software and preparation of derivative works
 * based upon this software are permitted. Any copy of this software or
 * of any derivative work must include the above copyright notice of
 * the author, this paragraph and the one after it.
 * 
 * This software is made available AS IS, and THE AUTHOR DISCLAIMS
 * ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, AND NOT WITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN,
 * ANY LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF THE AUTHOR IS ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 * 
 * All Rights Reserved.
 */
package bibex.extractor;

import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import bibex.dom.BibtexConcatenatedValue;
import bibex.dom.BibtexEntry;
import bibex.dom.BibtexField;
import bibex.dom.BibtexFile;
import bibex.dom.BibtexKey;
import bibex.dom.BibtexPreamble;
import bibex.dom.BibtexString;
import bibex.dom.BibtexStringDefinition;
import bibex.dom.BibtexStringReference;
import bibex.dom.BibtexToplevelComment;

/**
 * @author 	Yann-Ga�l Gu�h�neuc
 */
public class BibtexExtractor implements bibex.visitor.BibtexVisitor {
	private final Set typesSet;
	private final Set keysSet;
	private final PrintWriter writer;
	private final Map keyValueMap;
	private final ExtractorFunction function;

	private boolean entryTypeMatches;
	private boolean entryKeyMatches;
	private boolean keyValueMatches;

	public BibtexExtractor(
		final PrintWriter writer,
		final Set typesSet,
		final Set keysSet,
		final Map keyValueMap,
		final ExtractorFunction function) {

		this.writer = writer;
		this.typesSet = typesSet;
		this.keysSet = keysSet;
		this.keyValueMap = keyValueMap;
		this.function = function;
	}
	public void close(final BibtexEntry bibtexEntry) {
		if (this.function.isValid(
			this.entryTypeMatches,
			this.entryKeyMatches,
			this.keyValueMatches)) {

			this.writer.println(bibtexEntry);
		}
	}
	public void close(final BibtexField bibtexField) {
	}
	public void close(final BibtexFile bibtexFile) {
		this.writer.flush();
	}
	public void open(final BibtexEntry bibtexEntry) {
		this.entryKeyMatches = false;
		this.entryTypeMatches = false;
		this.keyValueMatches = false;

		final Iterator keysIterator = this.keysSet.iterator();
		if (keysIterator.hasNext()) {
			while (keysIterator.hasNext() && !this.entryKeyMatches) {
				final Matcher matcher =
					((Pattern) keysIterator.next()).matcher(bibtexEntry
						.getKey());

				if (matcher.matches()) {
					this.entryKeyMatches = true;
				}
			}
		}
		else {
			this.entryKeyMatches = true;
		}

		final Iterator typesIterator = this.typesSet.iterator();
		if (typesIterator.hasNext()) {
			while (typesIterator.hasNext() && !this.entryTypeMatches) {
				final Matcher matcher =
					((Pattern) typesIterator.next()).matcher(bibtexEntry
						.getType());

				if (matcher.matches()) {
					this.entryTypeMatches = true;
				}
			}
		}
		else {
			this.entryTypeMatches = true;
		}
	}
	public void open(final BibtexField bibtexField) {
		final String key = bibtexField.getKey().getValue();
		final String val = bibtexField.getValue().getValue();

		if (this.keyValueMap.containsKey(key)) {
			final Pattern pattern = (Pattern) this.keyValueMap.get(key);
			final Matcher matcher = pattern.matcher(val);

			if (matcher.matches()) {
				this.keyValueMatches = true;
			}
		}
	}
	public void open(final BibtexFile bibtexFile) {
	}
	public void visit(final BibtexConcatenatedValue bibtexConcatenatedValue) {
	}
	public void visit(final BibtexKey bibtexKey) {
	}
	public void visit(final BibtexPreamble bibtexPreamble) {
	}
	public void visit(final BibtexString bibtexString) {
	}
	public void visit(final BibtexStringDefinition bibtexStringDefinition) {
	}
	public void visit(final BibtexStringReference bibtexStringReference) {
	}
	public void visit(final BibtexToplevelComment bibtexToplevelComment) {
	}
}
