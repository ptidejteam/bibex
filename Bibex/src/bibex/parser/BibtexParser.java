/*
 * Created on Mar 19, 2003
 *
 * @author henkel@cs.colorado.edu
 * 
 */
package bibex.parser;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.LinkedList;
import bibex.dom.BibtexAbstractValue;
import bibex.dom.BibtexEntry;
import bibex.dom.BibtexFile;

/**
 * @author henkel
 */
public final class BibtexParser {

	private PseudoLexer lexer;
	private BibtexFile bibtexFile;
	private LinkedList exceptions;

	private final static char[] EXCEPTION_SET_NAMES =
		new char[] { '"', '#', '%', '\'', '(', ')', ',', '=', '{', '}' };
	//	private final static String[] ENTRY_TYPES =
	//		new String[] {
	//			"string",
	//			"preamble",
	//			"article",
	//			"book",
	//			"booklet",
	//			"conference",
	//			"inbook",
	//			"incollection",
	//			"inproceedings",
	//			"manual",
	//			"mastersthesis",
	//			"misc",
	//			"phdthesis",
	//			"proceedings",
	//			"techreport",
	//			"unpublished",
	//			"periodical" // not really standard but commonly used.
	//	};

	/**
	 * This is just for testing purposes. Look into bibtex.Main if you want to see an
	 * example that is kind of useful.
	 * 
	 * @param args
	 */

	public static void main(final String[] args) {
		final BibtexFile bibtexFile = new BibtexFile();
		final BibtexParser parser = new BibtexParser();
		try {
			final FileReader input = new FileReader(args[0]);

			parser.parse(bibtexFile, input, false);
		}
		catch (final Exception e) {
			System.err.println("Fatal Exception: ");
			e.printStackTrace();
			System.out.println("Fatal completion.");
			return;
		}
		finally {
			final ParseException[] exceptions = parser.getExceptions();
			if (exceptions.length > 0) {
				System.err.println("Non-fatal exceptions: ");
				for (int i = 0; i < exceptions.length; i++) {
					exceptions[i].printStackTrace();
					System.err.println("===================");
				}
				System.out.println("Almost normal completion.");
			}
		}
		//System.out.println("Dumping bibtexFile ... \n");
		//bibtexFile.printBibtex(new PrintWriter(System.out));
		System.out.println("Normal completion.");
	}

	/**
	 * returns the list of non-fatal exceptions that occured during parsing.
	 * Usually, these occur while parsing an entry. Usually, the remainder of the entry
	 * will be treated as part of a comment - thus the following entry will be parsed
	 * again.
	 *  
	 * @return List
	 */
	public ParseException[] getExceptions() {
		if (this.exceptions == null) {
			return new ParseException[0];
		}
		final ParseException[] result =
			new ParseException[this.exceptions.size()];
		this.exceptions.toArray(result);
		return result;
	}

	/**
	 * parses the input into bibtexFile - don't forget to check getExceptions()
	 * afterwards (if you use !throwAllParseExceptions)...
	 * 
	 * @param bibtexFile
	 * @param input
	 * @param throwAllParseExceptions if this is false, non-critical exceptions will
	 *                      be accumulated and can be retrieved using getExceptions().
	 * @throws ParseException
	 * @throws IOException
	 */

	public void parse(
		final BibtexFile bibtexFile,
		final Reader input,
		final boolean throwAllParseExceptions) throws ParseException,
			IOException {

		this.lexer = new PseudoLexer(input);
		this.bibtexFile = bibtexFile;
		this.exceptions = new LinkedList();
		while (true) {
			final PseudoLexer.Token token =
				this.lexer.scanTopLevelCommentOrAtOrEOF();
			switch (token.choice) {
				case 0 : // top level comment
					bibtexFile.addEntry(BibtexFile
						.makeToplevelComment(token.content));
					break;
				case 1 : // @ sign
					if (throwAllParseExceptions) {
						this.parseEntry();
					}
					else {
						try {
							this.parseEntry();
						}
						catch (final ParseException parseException) {
							this.exceptions.add(parseException);
						}
					}
					break;
				case 2 : // EOF
					return;
			}
		}
	}

	/**
	 * @return BibtexAbstractValue
	 */
	private BibtexAbstractValue parseBracketedString() throws ParseException,
			IOException {
		final String string = this.lexer.scanBracketedString().content;
		return BibtexFile.makeString(string.substring(0, string.length() - 1));
	}

	/**
	 * 
	 */
	private void parseEntry() throws ParseException, IOException {
		final String entryType = this.lexer.scanEntryTypeName().toLowerCase();
		final int bracketChoice =
			this.lexer.scanAlternatives(new char[] { '{', '(' }, false).choice;

		if (entryType.equals("string")) {
			final String stringName =
				this.lexer.scanLiteral(
					BibtexParser.EXCEPTION_SET_NAMES,
					true,
					true).content;
			this.lexer.scan('=');
			final BibtexAbstractValue value = this.parseValue();
			this.bibtexFile.addEntry(BibtexFile.makeStringDefinition(
				stringName,
				value));
		}
		else if (entryType.equals("preamble")) {
			final BibtexAbstractValue value = this.parseValue();
			this.bibtexFile.addEntry(BibtexFile.makePreamble(value));
		}
		else {
			// All others
			final String bibkey =
				this.lexer.scanLiteral(new char[] { ',' }, true, true).content;
			final BibtexEntry entry = BibtexFile.makeEntry(entryType, bibkey);
			this.bibtexFile.addEntry(entry);
			while (true) {
				this.lexer.enforceNoEof(
					"',' or corresponding closing bracket",
					true);
				//System.out.println("---------->'"+lexer.currentInputChar()+"'");
				if (this.lexer.currentInputChar() == ',') {
					this.lexer.scan(',');
					this.lexer.enforceNoEof("'}' or [FIELDNAME]", true);
					if (this.lexer.currentInputChar() == '}') {
						break;
					}
					final String fieldName =
						this.lexer.scanLiteral(
							BibtexParser.EXCEPTION_SET_NAMES,
							true,
							true).content;
					this.lexer.scan('=');
					final BibtexAbstractValue value = this.parseValue();
					entry.addField(BibtexFile.makeField(fieldName, value));
				}
				else {
					break;
				}
			}
		}

		if (bracketChoice == 0) {
			this.lexer.scan('}');
		}
		else {
			this.lexer.scan(')');
		}
	}

	/**
	 * @return BibtexAbstractValue
	 */
	private BibtexAbstractValue parseQuotedString() throws IOException,
			ParseException {
		return BibtexFile.makeString(this.lexer.scanQuotedString().content);
	}

	/**
	 * 
	 */
	private BibtexAbstractValue parseValue() throws ParseException, IOException {
		this.lexer.enforceNoEof(
			"[STRING] or [STRINGREFERENCE] or [NUMBER]",
			true);
		final char inputCharacter = this.lexer.currentInputChar();
		BibtexAbstractValue result;

		if (inputCharacter == '"') {
			result = this.parseQuotedString();
		}
		else if (inputCharacter == '{') {
			result = this.parseBracketedString();
		}
		else {
			final String stringContent =
				this.lexer.scanLiteral(
					BibtexParser.EXCEPTION_SET_NAMES,
					false,
					true).content.trim();
			try {
				Integer.parseInt(stringContent);
				// we can parse it, so it's a number
				result = BibtexFile.makeString(stringContent);
			}
			catch (final NumberFormatException nfe) {
				result = BibtexFile.makeReference(stringContent);
			}
		}

		this.lexer.enforceNoEof("'#' or something else", true);
		if (this.lexer.currentInputChar() == '#') {
			this.lexer.scan('#');
			return BibtexFile.makeConcatenatedValue(result, this.parseValue());
		}
		else {
			return result;
		}

	}
}
