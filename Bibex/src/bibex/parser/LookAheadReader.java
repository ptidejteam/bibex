/*
 * Created on Mar 19, 2003
 *
 * @author henkel@cs.colorado.edu
 * 
 */
package bibex.parser;

import java.io.IOException;
import java.io.Reader;

/**
 * initial implementation - inefficient!
 * 
 * @author henkel
 */
final class LookAheadReader {

	private final Reader input;

	private boolean eof;
	private int currentChar;
	private int line, column;
	public LookAheadReader(final Reader input) throws IOException {
		this.input = input;
		this.currentChar = input.read();
		this.eof = false;
		this.line = 1;
		this.column = 0;
	}

	public boolean eof() {
		return this.eof;
	}

	public int getColumn() {
		return this.column;
	}

	public char getCurrent() {
		// assert(currentChar>=0);
		return (char) this.currentChar;
	}

	public int getLine() {
		return this.line;
	}
	public void step() throws IOException {
		if (this.eof) {
			return;
		}
		this.currentChar = this.input.read();
		if (this.currentChar == '\n') {
			this.line++;
			this.column = 0;
		}
		else {
			this.column++;
		}
		if (this.currentChar == -1) {
			this.input.close();
			this.eof = true;
		}
	}

}
