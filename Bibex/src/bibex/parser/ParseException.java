/*
 * Created on Mar 19, 2003
 *
 * @author henkel@cs.colorado.edu
 * 
 */
package bibex.parser;

/**
 * @author henkel
 */
public final class ParseException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8876608094366528401L;

	ParseException(
		final int line,
		final int column,
		final String encountered,
		final String expected) {
		super("" + line + ":" + column + ": encountered '" + encountered
				+ "', expected '" + expected + "'.");
	}

}
