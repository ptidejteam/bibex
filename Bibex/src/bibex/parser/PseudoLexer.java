/*
 * Created on Mar 19, 2003
 *
 * @author henkel@cs.colorado.edu
 * 
 */
package bibex.parser;

import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;

/**
 * this is not a real lexer, since bibtex is such an insane format...
 * 
 * @author henkel
 */
final class PseudoLexer {

	static final class Token {
		final int choice;

		final String content;
		final int line, column;
		Token(
			final int choice,
			final String content,
			final int line,
			final int column) {
			this.choice = choice;
			this.content = content;
			this.line = line;
			this.column = column;
		}
	}

	private static String alternativesToString(final char[] alternatives) {
		final StringBuffer buffer = new StringBuffer();
		buffer.append("one of ");
		for (int i = 0; i < alternatives.length; i++) {
			if (i != 0) {
				buffer.append(',');
			}
			buffer.append('\'');
			buffer.append(alternatives[i]);
			buffer.append('\'');
		}

		return buffer.toString();
	}
	private static String alternativesToString(final Object[] alternatives) {
		final StringBuffer buffer = new StringBuffer();
		buffer.append("one of ");
		for (int i = 0; i < alternatives.length; i++) {
			if (i != 0) {
				buffer.append(',');
			}
			buffer.append('\'');
			buffer.append(alternatives[i]);
			buffer.append('\'');
		}

		return buffer.toString();
	}

	private static int index(final char[] container, final char element) {
		for (int i = 0; i < container.length; i++) {
			if (container[i] == element) {
				return i;
			}
		}
		return -1;

	}

	private final LookAheadReader input;

	private Token eofToken = null;

	PseudoLexer(final Reader input) throws IOException {
		this.input = new LookAheadReader(input);
	}

	/**
	 * make sure to query enforceNoEof first!
	 * @return char
	 */
	public char currentInputChar() {
		return this.input.getCurrent();
	}

	/**
	 * make sure you call
	 * @return boolean
	 */
	public void enforceNoEof(final String expected, final boolean skipWhiteSpace)
			throws ParseException, IOException {
		if (skipWhiteSpace) {
			this.skipWhitespace();
		}
		else if (this.input.eof()) {
			this.eofToken =
				new Token(
					-1,
					null,
					this.input.getLine(),
					this.input.getColumn());
		}
		if (this.eofToken != null) {
			throw new ParseException(
				this.eofToken.line,
				this.eofToken.column,
				"[EOF]",
				"" + expected);
		}
	}

	Token getNextToken() {
		return null;
	}

	public void scan(final char expected) throws ParseException, IOException {
		this.skipWhitespace();
		if (this.eofToken != null) {
			throw new ParseException(
				this.eofToken.line,
				this.eofToken.column,
				"[EOF]",
				"" + expected);
		}
		final char encountered = this.input.getCurrent();
		final int line = this.input.getLine(), column = this.input.getColumn();
		this.input.step();
		if (encountered != expected) {
			throw new ParseException(line, column, "" + encountered, ""
					+ expected);
		}
	}

	/**
	 * result.choice contains index into alternatives. If lookAhead is true we will not move
	 * forward ...
	 * 
	 * @param alternatives
	 * @return Token
	 */
	public Token scanAlternatives(
		final char[] alternatives,
		final boolean lookAhead) throws IOException, ParseException {
		this.skipWhitespace();
		if (this.eofToken != null) {
			throw new ParseException(
				this.eofToken.line,
				this.eofToken.column,
				"[EOF]",
				PseudoLexer.alternativesToString(alternatives));
		}
		final int line = this.input.getLine(), column = this.input.getColumn();
		for (int i = 0; i < alternatives.length; i++) {
			if (alternatives[i] == this.input.getCurrent()) {
				final Token result =
					new Token(
						i,
						"" + alternatives[i],
						this.input.getLine(),
						this.input.getColumn());
				if (!lookAhead) {
					this.input.step();
				}
				return result;
			}
		}
		if (!lookAhead) {
			this.input.step();
		}
		throw new ParseException(
			line,
			column,
			"" + this.input.getCurrent(),
			PseudoLexer.alternativesToString(alternatives));
	}

	/**
	 * this one is case insensitive!
	 * 
	 * @param alternatives
	 * @return Token
	 * @throws ParseException
	 * @throws IOException
	 */

	public Token scanAlternatives(final String[] alternatives)
			throws ParseException, IOException {
		this.skipWhitespace();
		if (this.eofToken != null) {
			throw new ParseException(
				this.eofToken.line,
				this.eofToken.column,
				"[EOF]",
				PseudoLexer.alternativesToString(alternatives));
		}
		final int line = this.input.getLine();
		final int column = this.input.getColumn();
		final HashMap amap = new HashMap();
		int maxLength = 0;
		for (int i = 0; i < alternatives.length; i++) {
			amap.put(alternatives[i], new Integer(i));
			if (alternatives[i].length() > maxLength) {
				maxLength = alternatives[i].length();
			}
		}
		String content = "";
		String lowerCaseContent = "";
		for (int length = 1; length <= maxLength; length++) {
			content += this.input.getCurrent();
			lowerCaseContent += Character.toLowerCase(this.input.getCurrent());
			this.input.step();

			if (amap.containsKey(lowerCaseContent)) {
				return new Token(
					((Integer) amap.get(lowerCaseContent)).intValue(),
					content,
					line,
					column);
			}
		}
		throw new ParseException(
			line,
			column,
			content,
			PseudoLexer.alternativesToString(alternatives));
	}

	public Token scanBracketedString() throws ParseException, IOException {
		final int line = this.input.getLine(), column = this.input.getColumn();
		final StringBuffer content = new StringBuffer();
		this.scan('{');
		int numberOfEnclosedBrackets = 0;
		while (true) {
			final Token stringbody =
				this.scanLiteral(new char[] { '}', '{' }, false, false);
			content.append(stringbody.content);
			if (stringbody.choice == 0) { // we terminated with '}'
				if (numberOfEnclosedBrackets > 0) {
					// content.append('}');
					numberOfEnclosedBrackets--;
				}
				else {
					content.append('}');
					break;
				}
			}
			else { // we terminated with '{'
				content.append('{');
				numberOfEnclosedBrackets++;
				content.append(this.scanBracketedString().content);
			}
		}
		this.scan('}');
		return new Token(0, content.toString(), line, column);
	}

	public String scanEntryTypeName() throws ParseException, IOException {
		this.skipWhitespace();
		if (this.eofToken != null) {
			throw new ParseException(
				this.eofToken.line,
				this.eofToken.column,
				"[EOF]",
				"[a..z,A..Z]");
		}
		final int line = this.input.getLine(), column = this.input.getColumn();
		final StringBuffer result = new StringBuffer();
		while (true) {
			this.enforceNoEof("[a..z,A..Z]", false);
			final char inputChar = this.input.getCurrent();

			if (inputChar >= 'a' && inputChar <= 'z' || inputChar >= 'A'
					&& inputChar <= 'Z') {
				result.append(inputChar);
				this.input.step();
			}
			else {
				break;
			}
		}
		if (result.length() == 0) {
			throw new ParseException(
				line,
				column,
				"" + this.input.getCurrent(),
				"[a..z,A..Z]");
		}
		return result.toString();

	}

	/**
	 * @return Token
	 */
	public Token scanLiteral(
		final char[] terminationSet,
		final boolean excludeWhitespace,
		final boolean enforceNonzero) throws ParseException, IOException {
		if (excludeWhitespace) {
			this.skipWhitespace();

			if (this.eofToken != null) {
				throw new ParseException(
					this.eofToken.line,
					this.eofToken.column,
					"[EOF]",
					"not (" + PseudoLexer.alternativesToString(terminationSet)
							+ " or [whitespace])");
			}
		}
		else {
			this.enforceNoEof(
				"not (" + PseudoLexer.alternativesToString(terminationSet)
						+ ")",
				false);
		}
		final int line = this.input.getLine(), column = this.input.getColumn();
		int indexIntoTerminationSet = -1;

		final StringBuffer content = new StringBuffer();
		while (true) {
			if (this.input.eof()) {
				break;
			}
			final char inputChar = this.input.getCurrent();

			indexIntoTerminationSet =
				PseudoLexer.index(terminationSet, inputChar);
			if (indexIntoTerminationSet >= 0 || excludeWhitespace
					&& Character.isWhitespace(inputChar)) {
				break;
			}
			else {
				this.input.step();
				content.append(inputChar);
			}
		}

		if (content.length() > 0 || !enforceNonzero) {
			return new Token(
				indexIntoTerminationSet,
				content.toString(),
				line,
				column);
		}
		else {
			throw new ParseException(
				line,
				column,
				"" + this.input.getCurrent(),
				"not (" + PseudoLexer.alternativesToString(terminationSet)
						+ " or [whitespace])");
		}
	}

	public Token scanQuotedString() throws IOException, ParseException {
		final int line = this.input.getLine(), column = this.input.getColumn();
		final StringBuffer content = new StringBuffer();
		this.scan('"');
		while (true) {
			// Yann 2014/05/23: Brackets in @STRINGs
			// There is no reason to prevent '{' in quoted strings:
			//	final Token stringbody =
			//		this.scanLiteral(new char[] { '\"', '{' }, false, false);
			final Token stringbody =
				this.scanLiteral(new char[] { '\"' }, false, false);
			content.append(stringbody.content);
			if (stringbody.choice == 0) { // we terminated with '"'
				break;
			}
			else { // we found a '{'
				content.append(this.scanBracketedString().content);
			}
		}
		this.scan('"');
		return new Token(0, content.toString(), line, column);
	}

	/**
	 * if it's a top level comment, result.choice will be 0, for @ 1, for EOF 2.
	 * for eof, result.choice will be -1. 
	 * @return Token
	 */
	public Token scanTopLevelCommentOrAtOrEOF() throws IOException {
		this.skipWhitespace();
		if (this.eofToken != null) {
			return new Token(
				2,
				this.eofToken.content,
				this.eofToken.line,
				this.eofToken.column);
		}

		final int column = this.input.getColumn(), line = this.input.getLine();
		if (this.input.getCurrent() == '@') {
			this.input.step();
			return new Token(1, "@", line, column);
		}
		final StringBuffer content = new StringBuffer();
		while (!this.input.eof() && this.input.getCurrent() != '@') {
			content.append(this.input.getCurrent());
			this.input.step();
		}
		return new Token(0, content.toString(), line, column);
	}

	private void skipWhitespace() throws IOException {
		if (this.eofToken != null) {
			return;
		}
		while (!this.input.eof()
				&& Character.isWhitespace(this.input.getCurrent())) {
			this.input.step();
		}
		if (this.input.eof()) {
			this.eofToken =
				new Token(
					-1,
					null,
					this.input.getLine(),
					this.input.getColumn());
		}
	}
}
