Bibex, BibTeX Extractor, is a 100%-pure Java program that automates the
extraction of bibliographic references from BibTeX databases using regular
expressions. Using Bibex user interface, it is possible to define, to save,
and to perform BibTeX references extraction tasks easily and automatically.
Bibex is very useful when using bibtopic or similar packages!